/*
* Name          : advisr_PrimaryCampaignTest
* Author        : Shift CRM
* Description   : Apex Unit Test for the advisr_PrimaryCampaignHandler.cls & advisr_CampaignTrigger.trigger
*
* Maintenance History:
* Date ------------ Name  ----  Version --- Remarks
* 1/29/2021     Ryan Morden       0.1       Initial
*/
@isTest
public class advisr_PrimaryCampaignTest {

    public static testMethod void testPrimaryInsert() {
        List<advisr_Campaign__c> assertCampaignLst = new List<advisr_Campaign__c>();

        //AC1 Should be flagged as Primary in State v1
        assertCampaignLst = new List<advisr_Campaign__c>([SELECT Id, Opportunity__c, Advisr_Campaign_Is_Primary__c, Advisr_Campaign_Is_Deleted__c FROM advisr_Campaign__c WHERE Advisr_Campaign_Name__c='ac1']);
        system.assertequals(assertCampaignLst.size(), 1);
        system.assertequals(assertCampaignLst[0].Advisr_Campaign_Is_Primary__c, true);

        //1 OF AC2 or AC3 Should be flagged as Primary in State v2
        assertCampaignLst = new List<advisr_Campaign__c>([SELECT Id, Opportunity__c, Advisr_Campaign_Is_Primary__c, Advisr_Campaign_Is_Deleted__c FROM advisr_Campaign__c 
                                                                        WHERE (Advisr_Campaign_Name__c='ac2' OR Advisr_Campaign_Name__c='ac3') AND Advisr_Campaign_Is_Primary__c=TRUE]);
        system.assertequals(assertCampaignLst.size(), 1);
    }


    public static testMethod void testPrimaryLinking() {
        List<advisr_Campaign__c> assertCampaignLst = new List<advisr_Campaign__c>();
        Opportunity OppV1 = [SELECT Id FROM Opportunity WHERE StageName='v1'];
        Opportunity OppV2 = [SELECT Id FROM Opportunity WHERE StageName='v2'];

        //Unlink and Relink AC1 TO OppV2 from Stage v2
        assertCampaignLst = new List<advisr_Campaign__c>([SELECT Id, Opportunity__c, Advisr_Campaign_Is_Primary__c, Advisr_Campaign_Is_Deleted__c FROM advisr_Campaign__c WHERE Advisr_Campaign_Name__c='ac1']);
        assertCampaignLst[0].Opportunity__c = OppV2.Id;
        update assertCampaignLst[0]; 
        assertCampaignLst = new List<advisr_Campaign__c>([SELECT Id, Opportunity__c, Advisr_Campaign_Is_Primary__c, Advisr_Campaign_Is_Deleted__c FROM advisr_Campaign__c WHERE Advisr_Campaign_Name__c='ac1']);
        system.assertequals(assertCampaignLst[0].Advisr_Campaign_Is_Primary__c, true);

        //Make sure there is only 1 Primary remaining
        assertCampaignLst = new List<advisr_Campaign__c>([SELECT Id, Opportunity__c, Advisr_Campaign_Is_Primary__c, Advisr_Campaign_Is_Deleted__c FROM advisr_Campaign__c WHERE Advisr_Campaign_Is_Primary__c=true]);
        system.assertequals(assertCampaignLst.size(), 1);
 

        //Relink current Primary to OppV1
        assertCampaignLst[0].Opportunity__c = OppV1.Id;
        update assertCampaignLst[0]; 
        
        //Make sure there are now 2 Primaries
        assertCampaignLst = new List<advisr_Campaign__c>([SELECT Id, Opportunity__c, Advisr_Campaign_Is_Primary__c, Advisr_Campaign_Is_Deleted__c FROM advisr_Campaign__c WHERE Advisr_Campaign_Is_Primary__c=true]);
        system.assertequals(assertCampaignLst.size(), 2);        
    }   
    
 
    public static testMethod void testPrimaryDelete() {
        List<advisr_Campaign__c> assertCampaignLst = new List<advisr_Campaign__c>();
        Opportunity OppV1 = [SELECT Id FROM Opportunity WHERE StageName='v1'];
        Opportunity OppV2 = [SELECT Id FROM Opportunity WHERE StageName='v2'];

        //Delete AC1 and verify no remaining Primary
        assertCampaignLst = new List<advisr_Campaign__c>([SELECT Id, Opportunity__c, Advisr_Campaign_Is_Primary__c, Advisr_Campaign_Is_Deleted__c FROM advisr_Campaign__c]);
        for (advisr_Campaign__c ac: assertCampaignLst) {
            ac.Advisr_Campaign_Is_Deleted__c = true;
        }
        update assertCampaignLst; 

        assertCampaignLst = new List<advisr_Campaign__c>([SELECT Id, Opportunity__c, Advisr_Campaign_Is_Primary__c, Advisr_Campaign_Is_Deleted__c FROM advisr_Campaign__c]);
        for (advisr_Campaign__c ac: assertCampaignLst) {
            system.assertequals(ac.Advisr_Campaign_Is_Primary__c, false);  
        } 
    }      


    @testSetup static void createTestData() {

        advisr_Group__c ag = new advisr_Group__c(
            Advisr_Group_Name__c = 'ag'
        );
        Insert ag; 
		
        Advisr_Client__c ac = new Advisr_Client__c(
            Advisr_Client_Name__c = 'ac',
			Advisr_Group__c = ag.Id
        );
        Insert ac; 

        //Initial state v1
        Opportunity tempOpp1 = new Opportunity(
            Name = 'Temp Opp v1', 
            StageName = 'v1',
            CloseDate=Date.Today()
        );
        Insert tempOpp1; 

        advisr_Campaign__c ac1 = new advisr_Campaign__c(
            Advisr_Campaign_Name__c = 'ac1',
            Opportunity__c = tempOpp1.Id, 
            Advisr_Client__c = ac.id,
            Advisr_Campaign_Is_Primary__c = false,
            Advisr_Campaign_Is_Deleted__c = false
        );
        Insert ac1;

        //Initial state v2
        Opportunity tempOpp2 = new Opportunity(
            Name = 'Temp Opp v2', 
            StageName = 'v2',
            CloseDate=Date.Today()+1
        );
        Insert tempOpp2;         

        advisr_Campaign__c ac2 = new advisr_Campaign__c(
            Advisr_Campaign_Name__c = 'ac2',
            Opportunity__c = tempOpp2.Id, 
            Advisr_Client__c = ac.id,
            Advisr_Campaign_Is_Primary__c = false,
            Advisr_Campaign_Is_Deleted__c = false
        );
        Insert ac2;        

        advisr_Campaign__c ac3 = new advisr_Campaign__c(
            Advisr_Campaign_Name__c = 'ac3',
            Opportunity__c = tempOpp2.Id, 
            Advisr_Client__c = ac.id,
            Advisr_Campaign_Is_Primary__c = false,
            Advisr_Campaign_Is_Deleted__c = false
        );
        Insert ac3;     
 

   }   

}