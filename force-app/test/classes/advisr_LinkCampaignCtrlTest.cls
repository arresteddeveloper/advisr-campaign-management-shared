/*
 * Name          : advisr_LinkCampaignCtrlTest
 * Author        : Shift CRM
 * Description   : Test class for advisr_LinkCampaignCtrl
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
@isTest
private with sharing class advisr_LinkCampaignCtrlTest {
    @TestSetup
    static void makeData(){
        insert new Advisr_Group__c(
            Advisr_Group_Name__c = 'Foo'
        );
    }
    /**
     * The following tests are for code coverage purposes only. 
     * NO additional functionality is added to these beyond what is already fully tested in the respective selector classes
     */
    static testMethod void test_searchClients() {
        System.assertEquals(0, advisr_LinkCampaignCtrl.searchClients('Hello', null, null).size());
    }
    
    static testMethod void test_searchAdvisrClientCampaigns() {
        System.assertEquals(0, advisr_LinkCampaignCtrl.searchAdvisrClientCampaigns('Hello', null, null).size());
    }

    static testMethod void test_getRecentlyViewedGroups() {
        advisr_LinkCampaignCtrl.getRecentlyViewedGroups();
    }

    static testMethod void test_searchGroups() {
        System.assertEquals(1, advisr_LinkCampaignCtrl.searchGroups('Foo', null).size());
    }
}
