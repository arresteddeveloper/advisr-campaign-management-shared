/*
 * Name          : advisr_ClientSelectorTest
 * Author        : Shift CRM
 * Description   : Test class for advisr_ClientSelector
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
@isTest
private class advisr_ClientSelectorTest {
    static testMethod void test_config() {
        advisr_ClientSelector selector = new advisr_ClientSelector();
        System.assertEquals(Advisr_Client__c.SobjectType, selector.getSObjectType());
    }

    static testMethod void test_searchAdvisrClientsByNameAndGroup() {
        List<Advisr_Client__c> clients = createClients();
        Opportunity opp = [SELECT Id FROM Opportunity];
        Advisr_Group__c groupA = [SELECT Id FROM Advisr_Group__c];
        
        advisr_ClientSelector selector = new advisr_ClientSelector();

        //There should be results when searching just by Group ID, but only one since the other doesn't have campaigns
        System.assertEquals(1, selector.searchAdvisrClientsByNameAndGroup(null, groupA.Id, '123').size());
        
        //There should be results when searching properly
        System.assertEquals(1, selector.searchAdvisrClientsByNameAndGroup('Foo', null, '123').size());

        //There should be results when searching based on group
        System.assertEquals(1, selector.searchAdvisrClientsByNameAndGroup('Foo', groupA.Id, '123').size());

        //There should be no results if there are no campaigns for a client
        System.assertEquals(0, selector.searchAdvisrClientsByNameAndGroup('Bar', groupA.Id, '123').size());

        //There should be no results if there are no campaigns NOT related to the provided opp
        System.assertEquals(0, selector.searchAdvisrClientsByNameAndGroup('Foo', groupA.Id, opp.Id).size());
    }

    static List<Advisr_Client__c> createClients() {
        Opportunity opp = new Opportunity(
            Name = 'Temp Opp v1', 
            StageName = 'v1',
            CloseDate=Date.Today()
        );
        insert opp;  

        Advisr_Group__c groupA = new Advisr_Group__c(
            //Name = 'Group A'
            Advisr_Group_Name__c = 'Foo'
        );
        insert groupA;

        List<Advisr_Client__c> clients = new List<Advisr_Client__c>();
        Advisr_Client__c client = new Advisr_Client__c(
            Advisr_Client_Id__c = '123',
            Advisr_Client_Name__c = 'Foo',
            Advisr_Group__c = groupA.Id
        );
        clients.add(client);

        Advisr_Client__c client2 = new Advisr_Client__c(
            Advisr_Client_Id__c = '1234',
            Advisr_Client_Name__c = 'Bar',
            Advisr_Group__c = groupA.Id
        );
        clients.add(client2);
        insert clients;

        List<Advisr_Campaign__c> campaigns = new List<Advisr_Campaign__c>();
        campaigns.add(new Advisr_Campaign__c(
            Advisr_Campaign_Name__c = 'Hello',
            Advisr_Campaign_Id__c = 123,
            Advisr_Campaign_Is_Primary__c = true,
            Advisr_Campaign_Is_Deleted__c = false,
            Advisr_Client__c = client.Id,
            Opportunity__c = opp.Id
        ));
        insert campaigns;
        return clients;
    }
}