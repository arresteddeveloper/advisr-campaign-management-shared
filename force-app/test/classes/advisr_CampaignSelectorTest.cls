/*
 * Name          : advisr_CampaignSelectorTest
 * Author        : Shift CRM
 * Description   : Test class for advisr_CampaignSelector
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
@isTest
private class advisr_CampaignSelectorTest {
    static testMethod void test_config() {
        advisr_CampaignSelector selector = new advisr_CampaignSelector();
        System.assertEquals(Advisr_Campaign__c.SobjectType, selector.getSObjectType());
    }
    static testMethod void test_getAdvisrCampaignsById() {
        //Retrieve campaigns by ID
        List<Advisr_Campaign__c> campaigns = createCampaigns();

        List<Id> idList = new List<Id>();
        for (Advisr_Campaign__c campaign : campaigns) {
            idList.add(campaign.Id);
        }

        List<Advisr_Campaign__c> retrievedCampaigns = new advisr_CampaignSelector().selectSObjectsById(idList);
        System.assertEquals(campaigns.size(), retrievedCampaigns.size());
    }

    static testMethod void test_getActiveAdvisrCampaignsByOppId() {
        //Since we aren't creating opps, it should return ones with a null oppId
        List<Advisr_Campaign__c> campaigns = createCampaigns();

        List<Id> idList = new List<Id>();
        for (Advisr_Campaign__c campaign : campaigns) {
            idList.add(campaign.Id);
        }

        List<Advisr_Campaign__c> campaignsByOpp = new advisr_CampaignSelector().getActiveAdvisrCampaignsByOppId(null);
        System.assertEquals(campaigns.size(), campaignsByOpp.size());

        List<Advisr_Campaign__c> allCampaigns = new advisr_CampaignSelector().getAllAdvisrCampaignsByOppId(null);
        System.assertEquals(campaigns.size(), allCampaigns.size());
    }

    static testMethod void test_getRemainingActiveCampaigns() {
        List<Advisr_Campaign__c> campaigns = createCampaigns();

        List<Advisr_Campaign__c> remainingCampaigns = new advisr_CampaignSelector().getRemainingActiveCampaigns(null, new List<id>());
        System.assertEquals(campaigns.size(), remainingCampaigns.size());

        remainingCampaigns = new advisr_CampaignSelector().getRemainingActiveCampaigns(null, new List<id>{ campaigns[0].id });
        System.assert(remainingCampaigns.size() == 0, 'There are more campaigns than expected');
    }

    static testMethod void test_searchCampaignsByNameAndClient() {
        List<Advisr_Campaign__c> campaigns = createCampaigns();
        
        Advisr_Client__c client = [SELECT Id FROM Advisr_Client__c LIMIT 1];
        advisr_CampaignSelector selector = new advisr_CampaignSelector();

        //There should be results when no search is applied
        System.assertEquals(campaigns.size(), selector.searchCampaignsByNameAndClient(null, client.Id, '123').size());
        
        //There should be results when searching properly
        System.assertEquals(campaigns.size(), selector.searchCampaignsByNameAndClient('Hello', client.Id, '123').size());

        //There should not be results when using an improper search term
        System.assertEquals(0, selector.searchCampaignsByNameAndClient('NO_RESULTS', client.Id, '123').size());
    }

    static List<Advisr_Campaign__c> createCampaigns() {
        advisr_Group__c ag = new advisr_Group__c(
            Advisr_Group_Name__c = 'ag'
        );
        Insert ag;
        
        Advisr_Client__c client = new Advisr_Client__c(
            Advisr_Client_Name__c = 'Foo',
			Advisr_Group__c = ag.Id
        );
        insert client;

        List<Advisr_Campaign__c> campaigns = new List<Advisr_Campaign__c>();
        campaigns.add(new Advisr_Campaign__c(
            Advisr_Campaign_Name__c = 'Hello',
            Advisr_Campaign_Id__c = 123,
            Advisr_Campaign_Is_Primary__c = true,
            Advisr_Campaign_Is_Deleted__c = false,
            Advisr_Client__c = client.Id
        ));
        insert campaigns;
        return campaigns;
    }
}
