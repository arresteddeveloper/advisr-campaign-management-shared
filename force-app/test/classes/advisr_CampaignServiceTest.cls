/*
 * Name          : advisr_CampaignServiceTest
 * Author        : Shift CRM
 * Description   : Test class for advisr_CampaignService
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
@isTest
private with sharing class advisr_CampaignServiceTest {
    
    static testMethod void test_unlinkCampaigns() {
        List<Advisr_Campaign__c> campaigns = createCampaigns();

        advisr_CampaignService.unlinkCampaigns(new List<Advisr_Campaign__c>{ campaigns[0] }, campaigns[1]);
        System.assertEquals(false, campaigns[0].Advisr_Campaign_Is_Primary__c, 'The original campaign is still primary');
        System.assertEquals(null, campaigns[0].Opportunity__c, 'The campaign was not unlinked');
        System.assertEquals(true, campaigns[1].Advisr_Campaign_Is_Primary__c, 'The replacement campaign was not flagged as primary');
    }
    
    static List<Advisr_Campaign__c> createCampaigns() {
        Opportunity opp = new Opportunity(
            Name = 'Temp Opp v1', 
            StageName = 'v1',
            CloseDate=Date.Today()
        );
        Insert opp;

        advisr_Group__c ag = new advisr_Group__c(
            Advisr_Group_Name__c = 'ag'
        );
        Insert ag;
        
        Advisr_Client__c client = new Advisr_Client__c(
            Advisr_Client_Name__c = 'Foo',
			Advisr_Group__c = ag.Id
        );
        insert client;

        List<Advisr_Campaign__c> campaigns = new List<Advisr_Campaign__c>();
        campaigns.add(new Advisr_Campaign__c(
            Advisr_Campaign_Name__c = 'Hello',
            Advisr_Campaign_Id__c = 123,
            Advisr_Campaign_Is_Primary__c = true,
            Advisr_Campaign_Is_Deleted__c = false,
            Opportunity__c = opp.Id,
            Advisr_Client__c = client.Id
        ));

        Advisr_Campaign__c secondCampaign = campaigns[0].clone(false, true, false, false);
        secondCampaign.Advisr_Campaign_Is_Primary__c = false;
        secondCampaign.Advisr_Campaign_Id__c = 234;
        campaigns.add(secondCampaign);
        insert campaigns;
        return campaigns;
    }
}
