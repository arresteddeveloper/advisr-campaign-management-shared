/*
 * Name          : advisr_UnlinkCampaignCtrlTest
 * Author        : Shift CRM
 * Description   : Test class for advisr_UnlinkCampaignCtrl
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
@IsTest
private with sharing class advisr_UnlinkCampaignCtrlTest {
    
    static testMethod void test_getAdvisrCampaignsById() {
        //Nothing really to test here since this just calls a selector
        List<Advisr_Campaign__c> campaigns = advisr_UnlinkCampaignCtrl.getAdvisrCampaignsById(new List<Id>());
        System.assertEquals(campaigns.size(), 0, 'Unexpected campaigns were returned');
    }
    static testMethod void test_getRemainingActiveCampaigns() {
        //Nothing really to call here since this just calls a Selector
        List<Advisr_Campaign__c> campaigns = advisr_UnlinkCampaignCtrl.getRemainingActiveCampaigns(null, new List<Id>());
        System.assertEquals(campaigns.size(), 0, 'Unexpected campaigns were returned');
    }
    static testMethod void test_unlinkCampaigns() {
        //Nothing to test here since this just calls a service method
        advisr_UnlinkCampaignCtrl.unlinkCampaigns(new List<Advisr_Campaign__c>(), null);
    }
}
