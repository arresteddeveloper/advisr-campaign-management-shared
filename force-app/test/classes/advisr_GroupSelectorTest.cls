/*
 * Name          : advisr_GroupSelectorTest
 * Author        : Shift CRM
 * Description   : Test class for advisr_GroupSelector
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
@isTest
private class advisr_GroupSelectorTest {
    static testMethod void test_config() {
        advisr_GroupSelector selector = new advisr_GroupSelector();
        System.assertEquals(Advisr_Group__c.SobjectType, selector.getSObjectType());
    }

    static testMethod void test_recentlyViewed() {
        //Since we aren't actually viewing these, there should be no results
        List<Advisr_Group__c> groups = createGroups();
        advisr_GroupSelector selector = new advisr_GroupSelector();

        //No assertinos here since RecentlyViewed is not controllable
        selector.getRecentlyViewedGroups();
    }

    static testMethod void test_searchAdvisrClientsByNameAndGroup() {
        List<Advisr_Group__c> groups = createGroups();
        advisr_GroupSelector selector = new advisr_GroupSelector();
        //There should be no results when no search is applied
        System.assertEquals(0, selector.searchAdvisrGroupsByName(null).size());
        
        //There should be results when searching properly
        System.assertEquals(1, selector.searchAdvisrGroupsByName('Group A').size());

        //There should be results when searching on a different value
        System.assertEquals(1, selector.searchAdvisrGroupsByName('Group B').size());

        //There should be no results when searching on an invalid value
        System.assertEquals(0, selector.searchAdvisrGroupsByName('NO_RESULTS').size());
    }

    static List<Advisr_Group__c> createGroups() { 
        Advisr_Group__c groupA = new Advisr_Group__c(
            //Name = 'Group A'
            Advisr_Group_Name__c = 'Group A'
        );

        Advisr_Group__c groupB = new Advisr_Group__c(
            //Name = 'Group B',
            Advisr_Group_Name__c = 'Group B'
        );
        List<Advisr_Group__c> groups = new List<Advisr_Group__c>();
        groups.add(groupA);
        groups.add(groupB);
        insert groups;
        return groups;
    }
}