import Advisr_linkCampaignStep from "c/advisr_linkCampaignStep";
import { getFieldValue, getRecord } from "lightning/uiRecordApi";
import { wire, api } from "lwc";
import { DatatableFieldBuilder } from "c/advisr_utils";

//Apex imports
import searchAdvisrClientCampaigns from '@salesforce/apex/advisr_LinkCampaignCtrl.searchAdvisrClientCampaigns'

//Schema imports
import ADVISR_CLIENT_NAME_FIELD from '@salesforce/schema/Advisr_Client__c.Advisr_Client_Name__c';
import ADVISR_CLIENT_GROUP_FIELD from '@salesforce/schema/Advisr_Client__c.Advisr_Group_Name__c';
import ADVISR_CAMPAIGN_ID_FIELD from '@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Id__c';
import ADVISR_CAMPAIGN_NAME_FIELD from '@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Name__c';
import ADVISR_CAMPAIGN_BUDGET_FIELD from '@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Budget__c';
import ADVISR_CAMPAIGN_CREATED_FIELD from '@salesforce/schema/Advisr_Campaign__c.CreatedDate';

//Label imports
import Advisr_No_Data_Message from '@salesforce/label/c.Advisr_No_Data_Message';


const DEBOUNCE_DELAY = 350;
export default class Advisr_linkCampaignStepTwo extends Advisr_linkCampaignStep {
  /***********************************
   * External API
   */
  @api clientId;
  @api opportunityId;

  @api validateStep () {
    return !!this.campaignId;
  }

  @api getStepData() {
    return {
      campaignId: this.campaignId
    }
  }

  /***********************************
   * Data retrieval/component variables
   */
  @wire(getRecord, {
    recordId: '$clientId',
    fields: [
      ADVISR_CLIENT_NAME_FIELD,
      ADVISR_CLIENT_GROUP_FIELD
    ]
  })
  client;

  @wire(searchAdvisrClientCampaigns, {
    clientId: '$clientId',
    searchTerm: '$searchTerm',
    opportunityId: '$opportunityId'
  })
  campaigns;

  //Loading indicator
  loading = false;

  //Search term used for additionally filtering campaigns
  searchTerm = null;

  //Timeout function stored for clearTimeout functionality
  delayTimeout;

  /************************************
   * Event handlers
   */
  handleInputChange(event) {
    window.clearTimeout(this.delayTimeout);
    // eslint-disable-next-line @lwc/lwc/no-async-operation
    this.delayTimeout = setTimeout(() => {
      this.searchTerm = event.detail.value
    }, DEBOUNCE_DELAY);
  }

  /************************************
   * Internal helper methods
   */
  getCamaignObjectInfo() {
    return this.getObjectInfo(this.ADVISR_CAMPAIGN_OBJECT);
  }

  /************************************
   * Dynamic properties
   */
  get campaignList() {
    return !!this.campaigns && this.campaigns.data ? this.campaigns.data : [];
  }

  get campaignId() {
    let dataTable = this.template.querySelector("lightning-datatable");
    if (!dataTable) return null;

    let selectedRows = dataTable.getSelectedRows();
    if (!selectedRows || selectedRows.length === 0) return null;


    return selectedRows[0].Id;
  }

  get clientName() {
    return getFieldValue(this.client.data, ADVISR_CLIENT_NAME_FIELD) + ', ' + getFieldValue(this.client.data, ADVISR_CLIENT_GROUP_FIELD);
  }

  get clientLink() {
    return `/${this.clientId}`;
  }

  get tableColumns() {
    if (!this.getCamaignObjectInfo()) return [];

    let clientObject = this.getCamaignObjectInfo();
    let builder = new DatatableFieldBuilder(clientObject);
    return builder.build([
      ADVISR_CAMPAIGN_ID_FIELD,
      ADVISR_CAMPAIGN_NAME_FIELD,
      ADVISR_CAMPAIGN_BUDGET_FIELD,
      ADVISR_CAMPAIGN_CREATED_FIELD
    ]);
  }

  get noData() {
    return this.campaignList && this.campaignList.length === 0 && !this.loading;
  }

  get noDataMessage() {
    return Advisr_No_Data_Message;
  }
}
