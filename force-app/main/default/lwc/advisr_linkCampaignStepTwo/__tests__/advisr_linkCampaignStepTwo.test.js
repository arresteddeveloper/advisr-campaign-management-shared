import { createElement } from 'lwc';
import Advisr_linkCampaignStepTwo from 'c/advisr_linkCampaignStepTwo';
import { getRecord } from "lightning/uiRecordApi";
import { registerApexTestWireAdapter, registerLdsTestWireAdapter } from '@salesforce/sfdx-lwc-jest';
import { getObjectInfos } from "lightning/uiObjectInfoApi";
import searchAdvisrClientCampaigns from "@salesforce/apex/advisr_LinkCampaignCtrl.searchAdvisrClientCampaigns";

// Mock realistic data
const mockGetRecord = require('./data/getRecord.json');
const mockGetClientCampaigns = require('./data/getClientCampaigns.json');

// Register as an LDS wire adapter. Some tests verify the provisioned values trigger desired behavior.
const getRecordAdapter = registerLdsTestWireAdapter(getRecord);
const getObjectInfosAdapter = registerLdsTestWireAdapter(getObjectInfos); //We don't use this, but since it is part of the inheritance chain make sure to mock it
const searchAdvisrClientCampaignsAdapter = registerApexTestWireAdapter(searchAdvisrClientCampaigns);

// Helper function to wait until the microtask queue is empty. This is needed for promise
// timing when calling imperative Apex.
function flushPromises() {
    // eslint-disable-next-line no-undef
    return new Promise((resolve) => setImmediate(resolve));
}

jest.useFakeTimers();

describe('c-advisr_link-campaign-step-two', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }

        jest.clearAllMocks();
    });

    const setup = () => {
        const element = createElement('c-advisr_link-campaign-step-two', {
            is: Advisr_linkCampaignStepTwo
        });
        element.clientId = '1';
        document.body.appendChild(element);
        return element;
    }
    
    it('should render the name of the selected client', () => {
        let element = setup();
        
        // Emit data from @wire
        getRecordAdapter.emit(mockGetRecord);

        return Promise.resolve().then(() => {
            expect(element.shadowRoot.querySelector('.clientName').innerHTML).toBe('Foo, Bar');
            expect(element.shadowRoot.querySelector('.clientName').href).toBe('http://localhost/1');
        })
    });

    it('should show a list of campaigns related to the client', () => {
        let APEX_PARAMS = {
            searchTerm: null,
            clientId: '1'
        }
        let element = setup();

        // Emit data from @wire
        getRecordAdapter.emit(mockGetRecord);
        searchAdvisrClientCampaignsAdapter.emit(mockGetClientCampaigns);

        return flushPromises().then(() => {
            let dataTable = element.shadowRoot.querySelector('lightning-datatable');
            expect(dataTable.data).toHaveLength(1);
        })
    })

    it('should show a notification when there is no data to show', () => {
        let element = setup();
        
        let message = element.shadowRoot.querySelector('c-advisr_no-records-illustration');
        expect(message).not.toBeNull();

        searchAdvisrClientCampaignsAdapter.emit([]);

        return flushPromises().then(() => {
            message = element.shadowRoot.querySelector('c-advisr_no-records-illustration');
            expect(message).not.toBeNull();
        })
    })
    
    describe('External API', () => {
        it('should validate to false when improperly configured', () => {
            let element = setup();
    
            expect(element.validateStep()).toBe(false);
        })
    
        it('should validate to true when properly configured', () => {
            let element = setup();

            searchAdvisrClientCampaignsAdapter.emit(mockGetClientCampaigns);
    
            // Fast-forward until all timers have been executed
            jest.runAllTimers();
    
            return flushPromises().then(() => {
                let table = element.shadowRoot.querySelector('lightning-datatable');
                table.selectedRows = [{"Id":"a010R00000AwrqQQAR","Advisr_Group__c":"123","Advisr_Client_Name__c":"Hello World","Advisr_Group_Name__c":"Group A","CreatedDate":"2021-02-02T21:31:49.000Z","campaignCount":0}];
                
                return Promise.resolve()
            }).then(() => {
                expect(element.validateStep()).toBe(true);
            })
        })

        it('should provide selected information in key => value format', () => {
            let element = setup();

            searchAdvisrClientCampaignsAdapter.emit(mockGetClientCampaigns);
    
            return flushPromises().then(() => {
                let table = element.shadowRoot.querySelector('lightning-datatable');
                table.selectedRows = [{"Id":"a010R00000AwrqQQAR","Advisr_Group__c":"123","Advisr_Client_Name__c":"Hello World","Advisr_Group_Name__c":"Group A","CreatedDate":"2021-02-02T21:31:49.000Z","campaignCount":0}];
                
                return Promise.resolve()
            }).then(() => {
                expect(element.getStepData()).toEqual({
                    campaignId: "a010R00000AwrqQQAR"
                });
            })
        })
    })
});