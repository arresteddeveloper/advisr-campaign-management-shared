import { createElement } from "lwc";
import Advisr_Modal from "c/advisr_Modal";

describe("c-advisr_-modal", () => {
  afterEach(() => {
    // The jsdom instance is shared across test cases in a single file so reset the DOM
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }
  });

  it("should show a loading indicator when true", () => {
    const element = createElement("c-advisr_-modal", {
      is: Advisr_Modal
    });
    element.loading = true;
    document.body.appendChild(element);

    let loadingSpinner = element.shadowRoot.querySelector("lightning-spinner");
    expect(loadingSpinner).not.toBeNull();
  });

  it("should not show a loading indicator when false", () => {
    const element = createElement("c-advisr_-modal", {
      is: Advisr_Modal
    });
    document.body.appendChild(element);

    let loadingSpinner = element.shadowRoot.querySelector("lightning-spinner");
    expect(loadingSpinner).toBeNull();
  });
});
