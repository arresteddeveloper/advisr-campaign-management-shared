export const getDataTypeFromField = (field) => {
  switch (field.dataType) {
    case "String":
    case "Picklist":
    case "TextArea":
      return { type: "text" };
    case "Currency":
      return {
        type: "currency",
        typeAttributes: {
          minimumFractionDigits: field.scale,
          maximumFractionDigits: field.scale
        }
      };
    case "Double":
      return {
        type: "number",
        typeAttributes: {
          minimumFractionDigits: field.scale,
          maximumFractionDigits: field.scale
        }
      };
    case "Boolean":
      return { type: "boolean" };
    case "DateTime":
      return { type: "date" };
    case "Date":
      return { type: "date-local" };
    default:
      throw new Error(
        `Unsupported datatype (${field.apiName}: ${field.dataType})`
      );
  }
};

export class DatatableFieldBuilder {
  objectInfo;

  constructor(objectInfo) {
    this.objectInfo = objectInfo;
  }

  build(fieldList = []) {
    let { fields: objectFields } = this.objectInfo;

    let columns = fieldList
      .filter((field) => (typeof objectFields[field.fieldApiName] !== undefined || field.isCustom))
      .map((field) => {
        //If using the optional isCustom override, just return the field
        if (field.isCustom) return field;

        let objectField = objectFields[field.fieldApiName];

        return {
          label: objectField.label,
          fieldName: objectField.apiName,
          hideDefaultActions: true,

          //Include the type and attributes
          ...getDataTypeFromField(objectField)
        };
      });
    return columns;
  }
}
