import { LightningElement, api, track } from "lwc";
import { classSet } from "c/advisr_utils";

export default class Advisr_toastMessage extends LightningElement {
  @track title;
  @track message;
  @track variant = "info";
  @track visible = false;

  //Internal property for tracking timeout
  closeTimeout;

  constructor() {
    super();
    this.template.addEventListener("advisr_showtoast", this.showToast);
  }

  @api show({ title, message, variant = "info" }) {
    this.title = title;
    this.message = message;
    this.variant = variant;

    this.visible = true;

    this.closeTimeout = setTimeout(() => {
      this.visible = false;
    }, 3000);
  }

  //**************************************************
  // Event handlers
  //*/
  showToast(event) {
    let { title, message, variant = "info" } = event.detail;
    this.title = title;
    this.message = message;
    this.variant = variant;

    this.visible = true;

    this.closeTimeout = setTimeout(() => {
      this.visible = false;
    }, 3000);
  }

  closeNotication() {
    clearTimeout(this.closeTimeout);
    this.visible = false;
  }

  get assistiveText() {
    return this.variant;
  }

  get computedVisibilityClass() {
    return classSet("slds-notify_container").add({
      "slds-hide": !this.visible
    });
  }

  get computedVariantClass() {
    return classSet("slds-notify slds-notify_toast").add({
      "slds-theme_success": this.variant === "success",
      "slds-theme_warning": this.variant === "warning",
      "slds-theme_error": this.variant === "error",
      "slds-theme_info": this.variant === "info"
    });
  }

  get computedIconName() {
    switch (this.variant) {
      case "success":
        return "utility:success";
      case "warning":
        return "utility:warning";
      case "error":
        return "utility:error";
      default:
        return "utility:info";
    }
  }
}
