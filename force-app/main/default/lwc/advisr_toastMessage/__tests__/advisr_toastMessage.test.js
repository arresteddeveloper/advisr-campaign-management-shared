import { createElement } from "lwc";
import Advisr_toastMessage from "c/advisr_toastMessage";

describe("c-advisr_toast-message", () => {
  afterEach(() => {
    // The jsdom instance is shared across test cases in a single file so reset the DOM
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }
  });

  let element;
  beforeEach(() => {
    element = createElement("c-advisr_toast-message", {
      is: Advisr_toastMessage
    });
    document.body.appendChild(element);
  });

  it("should appear when the proper event is called", () => {
    element.dispatchEvent(
      new CustomEvent("advisr_showtoast", {
        detail: {
          title: "Error",
          message: "Hello World"
        }
      })
    );

    return Promise.resolve(() => {
      let wrapper = element.shadowRoot.querySelector(".slds-notify_container");
      expect(wrapper.classList).not.toContain("slds-hide");
    });
  });
  it("should no show up until the proper event is called", () => {
    let wrapper = element.shadowRoot.querySelector(".slds-notify_container");
    expect(wrapper.classList).toContain("slds-hide");
  });
  it("should have an api method to show the toast", () => {
    element.show({ title: "Error", message: "Hello World" });
    return Promise.resolve(() => {
      let wrapper = element.shadowRoot.querySelector(".slds-notify_container");
      expect(wrapper.classList).not.toContain("slds-hide");
    });
  });
  it("should allow customizable variants", () => {
    element.dispatchEvent(
      new CustomEvent("advisr_showtoast", {
        detail: {
          title: "Error",
          message: "Hello World",
          variant: "error"
        }
      })
    );

    return Promise.resolve(() => {
      let wrapper = element.shadowRoot.querySelector(".slds-notify_toast");
      expect(wrapper.classList).toContain("slds-theme-error");
    });
  });
  it("should default to info variant", () => {
    element.dispatchEvent(
      new CustomEvent("advisr_showtoast", {
        detail: {
          title: "Error",
          message: "Hello World"
        }
      })
    );

    return Promise.resolve(() => {
      let wrapper = element.shadowRoot.querySelector(".slds-notify_toast");
      expect(wrapper.classList).toContain("slds-theme-info");
    });
  });

  it("should disappear after 3 seconds if no button is clicked", () => {
    jest.useFakeTimers();
    element.dispatchEvent(
      new CustomEvent("advisr_showtoast", {
        detail: {
          title: "Error",
          message: "Hello World"
        }
      })
    );

    jest.runAllTimers();

    return Promise.resolve(() => {
      let wrapper = element.shadowRoot.querySelector(".slds-notify_container");
      expect(wrapper.classList).toContain("slds-hide");
    });
  });

  it("should disappear when the user clicks close", () => {
    element.dispatchEvent(
      new CustomEvent("advisr_showtoast", {
        detail: {
          title: "Error",
          message: "Hello World"
        }
      })
    );

    let btn = element.shadowRoot.querySelector("lightning-button-icon");
    btn.click();

    return Promise.resolve(() => {
      let wrapper = element.shadowRoot.querySelector(".slds-notify_container");
      expect(wrapper.classList).toContain("slds-hide");
    });
  });
});
