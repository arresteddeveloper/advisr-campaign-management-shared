import { LightningElement, api } from "lwc";

//Custom labels
import Advisr_Link_Step_One_Title from "@salesforce/label/c.Advisr_Link_Step_One_Title";
import Advisr_Link_Step_Two_Title from "@salesforce/label/c.Advisr_Link_Step_Two_Title";
import Advisr_Link_Step_Three_Title from "@salesforce/label/c.Advisr_Link_Step_Three_Title";

export default class Advisr_linkCampaignAction extends LightningElement {
  //Related Opportunity ID used to navigate back to related list page
  @api opportunityId;

  //client used to filter the advisr_campaign records available for linking
  clientId;

  //selected advisr_campaign record used for linking
  campaignId;

  //The current step within the wizard
  currentStep = 1;
  currentStepString = '1';

  //The current loading status of any components
  loading = false;

  //The final step in the process, save is completed successfully
  saveSuccess = false;

  //**************************************************
  // Actions
  //*/
  handleLoadingChange(event) {
    this.loading = event.detail.isLoading;
  }

  next() {
    if (!this.validateCurrentStep()) return;
    this.getStepData();
    this.currentStep++;
    this.currentStepString = this.currentStep.toString();
  }

  previous() {
    this.currentStep--;
    this.currentStepString = this.currentStep.toString();
  }

  cancel() {
    window.location.assign(
      "/lightning/r/Opportunity/" + this.opportunityId + "/view"
    );
  }

  submit() {
    this.getCurrentStep().submit();
  }

  handleSuccess() {
    this.saveSuccess = true;
  }

  //**************************************************
  // Internal methods
  // */

  //Ensure that the step is valid before progressing to the next step
  validateCurrentStep() {
    let currentStep = this.getCurrentStep();
    if (!currentStep) return false;
    return currentStep.validateStep();
  }

  //Retrieve the data from the current step and map it to this object
  getStepData() {
    let currentStep = this.getCurrentStep();
    let stepData = currentStep.getStepData();

    Object.keys(stepData).forEach(key => {
      this[key] = stepData[key]
    });
  }

  //Return the DOM element corresponding to the current step
  getCurrentStep() {
    return this.template.querySelector('.current-step');
  }

  //**************************************************
  // Dynamic parameters
  //*/

  //Steps for the page
  //1. Select an Advisr_Client__c record
  //2. Select an Advisr_Campaign__c record
  //3. Confirmg Advisr_Campaign__c record information and update as needed
  get stepOneClass() {
    return this.currentStep === 1 ? 'current-step' : 'slds-hide';
  }
  get stepTwoClass() {
    return this.currentStep === 2 ? 'current-step': 'slds-hide';
  }
  get stepThreeClass() {
    return this.currentStep === 3 ? 'current-step' : 'slds-hide';
  }

  //Determine whether there are previous/next actions
  get hasNext() {
    return this.currentStep < 3 && !this.saveSuccess
  }

  get hasPrevious() {
    return this.currentStep > 1 && !this.saveSuccess
  }

  //Page title for the component
  get pageTitle() {
    switch (this.currentStep) {
      case 1:
        return Advisr_Link_Step_One_Title;
      case 2:
        return Advisr_Link_Step_Two_Title;
      case 3:
        return Advisr_Link_Step_Three_Title;
    }
  }

  get showSubmit() {
    return !this.saveSuccess && !this.hasNext;
  }

  get cancelLabel() {
    return this.saveSuccess ? 'Ok' : 'Cancel';
  }

  get isCurrentStepInvalid() {
    return !this.validateCurrentStep();
  }
}
