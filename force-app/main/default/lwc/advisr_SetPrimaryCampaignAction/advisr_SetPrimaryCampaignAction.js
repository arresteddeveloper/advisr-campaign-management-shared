import { LightningElement, api, wire, track } from "lwc";
import { reduceErrors } from "c/advisr_utils";
import { getFieldValue, getRecord, updateRecord } from "lightning/uiRecordApi";
import { NavigationMixin } from "lightning/navigation";

//Imported fields
import ID_FIELD from "@salesforce/schema/Advisr_Campaign__c.Id";
import NAME_FIELD from "@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Name__c";
import IS_PRIMARY_FIELD from "@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Is_Primary__c";

//Imported labels
import Advisr_Primary_Campaign_Success_Toast from "@salesforce/label/c.Advisr_Primary_Campaign_Success_Toast";
import Advisr_No_Records_Selected_Message from "@salesforce/label/c.Advisr_No_Records_Selected_Message";
import Advisr_Primary_Too_Many_Records_Selected from "@salesforce/label/c.Advisr_Primary_Too_Many_Records_Selected";
import Advisr_Selected_Already_Primary from "@salesforce/label/c.Advisr_Selected_Already_Primary";
import Advisr_Primary_Campaign_Success_Message from "@salesforce/label/c.Advisr_Primary_Campaign_Success_Message";
import Advisr_Primary_Campaign_Confirmation_Message from "@salesforce/label/c.Advisr_Primary_Campaign_Confirmation_Message";
import Advisr_Primary_Campaign_Component_Label from "@salesforce/label/c.Advisr_Primary_Campaign_Component_Label";

export default class Advisr_SetPrimaryCampaignAction extends NavigationMixin(
  LightningElement
) {
  //Array of record Ids passed from the VF page
  @api recordIds;

  //Related Opportunity ID used to navigate back to related list page
  @api opportunityId;

  //Optional comfigurable page title
  @api pageTitle = Advisr_Primary_Campaign_Component_Label;

  //Individual recordId, corresponds to the one selected assuming only one was selected by users
  @track recordId;

  //Retrieve the record, we will use this to update the value of the Is_Primary__c checkbox assuming everything is good
  @wire(getRecord, {
    recordId: "$recordId",
    fields: [NAME_FIELD, IS_PRIMARY_FIELD]
  })
  wiredRecord;

  //Variables to be used to manage page state
  saveSuccess;
  saveError;
  loading;

  //Lifecycle hook fires when the component is initialized
  connectedCallback() {
    //Only retrieve the record to set primary if there is only one selected
    if (this.recordIds && this.recordIds.length === 1) {
      this.recordId = this.recordIds[0];
    } else if (this.recordIds) {
      window.console.log(JSON.parse(JSON.stringify(this.recordIds)));
    }
  }

  //**************************************************
  // ACTIONS
  //*/

  //Return to the previous page
  cancel() {
    window.location.assign(
      "/lightning/r/Opportunity/" + this.opportunityId + "/view"
    );
  }

  apply() {
    this.loading = true;
    updateRecord({
      fields: {
        [ID_FIELD.fieldApiName]: this.recordId,
        [IS_PRIMARY_FIELD.fieldApiName]: true
      }
    })
      .then((results) => {
        this.template.querySelector("c-advisr_toast-message").show({
          variant: "success",
          message: Advisr_Primary_Campaign_Success_Toast.replace('{0}', this.name)
        });
        this.saveSuccess = true;
      })
      .catch((err) => {
        this.template.querySelector("c-advisr_toast-message").show({
          variant: "error",
          message: reduceErrors(err)[0]
        });
        this.saveError = reduceErrors(err)[0];
      })
      .finally(() => {
        this.loading = false;
      });
  }

  //**************************************************
  // Dynamic parameters
  //*/

  //Retrieve the error message if there is any
  //These are the following scenarios that may throw an error
  //1. Multiple campaigns have been selected as primary
  //2. No campaigns have been selected as primary
  //3. The selected campaign is already primary
  //4. There was an error loading the record
  //5. There was an error updating/saving the record
  get errorMessage() {
    if (this.saveSuccess) return null;
    if (!this.recordIds || this.recordIds.length === 0)
      return Advisr_No_Records_Selected_Message;
    if (this.recordIds && this.recordIds.length > 1)
      return Advisr_Primary_Too_Many_Records_Selected;
    if (this.isPrimary)
      return Advisr_Selected_Already_Primary;
    if (this.wiredRecord && this.wiredRecord.error)
      return this.wiredRecord.error;
    if (this.saveError) return this.saveError;
    return null;
  }

  //Retrieve the save success message when the record has been successfully updated
  get saveSuccessMessage() {
    if (!this.saveSuccess) return null;
    return Advisr_Primary_Campaign_Success_Message.replace('{0}', this.name);
  }

  //Retrieve the message shown to the user when confirming which campaign will be updated
  get confirmationMessage() {
    return Advisr_Primary_Campaign_Confirmation_Message.replace('{0}', this.name);
  }

  //Retrieve the campaign name
  get name() {
    return getFieldValue(this.wiredRecord.data, NAME_FIELD);
  }

  //Retrieve whether or not the campaign is already primary
  get isPrimary() {
    return getFieldValue(this.wiredRecord.data, IS_PRIMARY_FIELD);
  }

  //Determine whether or not to render the campaign information
  //This will show once a record has been successfully queried and if there are no other errors
  get showCampaignInformation() {
    return !!this.wiredRecord.data && !this.errorMessage && !this.saveSuccess;
  }
}
