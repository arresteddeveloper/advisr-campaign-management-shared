import { createElement } from "lwc";
import Advisr_SetPrimaryCampaignAction from "c/advisr_setPrimaryCampaignAction";
import { getRecord, updateRecord } from "lightning/uiRecordApi";
import { registerLdsTestWireAdapter } from "@salesforce/sfdx-lwc-jest";

const mockGetRecord = require("./data/getRecord.json");
const mockGetPrimaryRecord = require("./data/getPrimaryRecord.json");

// Register as an LDS wire adapter. Some tests verify the provisioned values trigger desired behavior.
const getRecordAdapter = registerLdsTestWireAdapter(getRecord);

jest.useFakeTimers()

describe("c-advisr_-set-primary-campaign-action", () => {
  afterEach(() => {
    // The jsdom instance is shared across test cases in a single file so reset the DOM
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }

    // Prevent data saved on mocks from leaking between tests
    jest.clearAllMocks();
  });

  const setup = (recordIds) => {
    const element = createElement("c-advisr_-set-primary-campaign-action", {
      is: Advisr_SetPrimaryCampaignAction
    });
    element.recordIds = recordIds;
    document.body.appendChild(element);
    return element;
  };

  // Helper function to wait until the microtask queue is empty. This is needed for promise
  // timing.
  function flushPromises() {
    // eslint-disable-next-line no-undef
    return new Promise((resolve) => setImmediate(resolve));
  }

  describe("Initialization", () => {
    it("Should show a page title, which should be configurable", () => {
      let element = setup();
      let title = element.shadowRoot.querySelector(".page-title");
      expect(title.textContent).toBe("c.Advisr_Primary_Campaign_Component_Label");

      element.pageTitle = "Hello World";

      Promise.resolve().then(() => {
        title = element.shadowRoot.querySelector(".page-title");
        expect(title.textContent).toBe("Hello World");
      });
    });

    it("Should show an error message when no record is selected", () => {
      let element = setup();
      const message = element.shadowRoot.querySelector(".error-message");
      expect(message).not.toBeNull();
    });

    it("Should show an error message when more than one record is selected", () => {
      let element = setup(["1", "2"]);
      const message = element.shadowRoot.querySelector(".error-message");
      expect(message).not.toBeNull();
    });

    it("Should show no errors when only one record is selected", () => {
      let element = setup(["1"]);
      const message = element.shadowRoot.querySelector(".error-message");
      expect(message).toBeNull();
    });

    it("Should render campaign name when configurated properly", () => {
      let element = setup(["1"]);

      // Emit data from @wire
      getRecordAdapter.emit(mockGetRecord);

      return flushPromises().then(() => {
        const nameEl = element.shadowRoot.querySelector(
          ".campaign-information"
        );
        expect(nameEl.textContent).toContain("c.Advisr_Primary_Campaign_Confirmation_Message");
      });
    });

    it("Should render an error when already primary", () => {
      let element = setup(["1"]);

      // Emit data from @wire
      getRecordAdapter.emit(mockGetPrimaryRecord);

      return flushPromises().then(() => {
        const message = element.shadowRoot.querySelector(".error-message");
        const campaignInformation = element.shadowRoot.querySelector(
          ".campaign-information"
        );
        expect(message).not.toBeNull();
        expect(campaignInformation).toBeNull();
      });
    });
  });

  describe("Actions", () => {
    it("Should show a cancel and submit button when the data is configured properly", () => {
      let element = setup(["1"]);
      getRecordAdapter.emit(mockGetRecord);

      return flushPromises().then(() => {
        let okButton = element.shadowRoot.querySelector(".ok-button");
        let submitButton = element.shadowRoot.querySelector(".apply-button");
        let cancelButton = element.shadowRoot.querySelector(".cancel-button");

        expect(okButton).toBeNull();
        expect(submitButton).not.toBeNull();
        expect(cancelButton).not.toBeNull();
      });
    });

    it("Should only show an OK button when there is an error", () => {
      let element = setup(["1"]);

      let okButton = element.shadowRoot.querySelector(".ok-button");
      expect(okButton).not.toBeNull();
    });

    it("Should return to the previous page when Cancel is clicked", () => {
      let assignMock = jest.fn();

      delete window.location;
      window.location = { assign: assignMock };

      let element = setup(["1"]);
      getRecordAdapter.emit(mockGetRecord);

      return flushPromises()
        .then(() => {
          const btn = element.shadowRoot.querySelector(".cancel-button");
          btn.click();

          return Promise.resolve();
        })
        .then(() => {
          expect(window.location.assign).toHaveBeenCalledTimes(1);
          assignMock.mockClear();
        });
    });

    describe("Save functionality", () => {
      it("should show a success message when apply is successful", () => {
        updateRecord.mockResolvedValue({});

        let element = setup(["1"]);
        getRecordAdapter.emit(mockGetRecord);

        return flushPromises()
          .then(() => {
            const btn = element.shadowRoot.querySelector(".apply-button");
            btn.click();

            return flushPromises();
          })
          .then(() => {
            let message = element.shadowRoot.querySelector(".save-success");
            expect(message).not.toBeNull();
          });
      });

      it("should show an error message when apply fails for any reason", () => {
        updateRecord.mockRejectedValue(new Error("Error Message"));

        let element = setup(["1"]);
        getRecordAdapter.emit(mockGetRecord);

        return flushPromises()
          .then(() => {
            const btn = element.shadowRoot.querySelector(".apply-button");
            btn.click();

            return flushPromises();
          })
          .then(() => {
            let message = element.shadowRoot.querySelector(".error-message");
            expect(message).not.toBeNull();
            expect(message.textContent).toContain("Error Message");
          });
      });

      it("should show a loading indicator while saving", () => {
        updateRecord.mockResolvedValue({});

        let element = setup(["1"]);
        getRecordAdapter.emit(mockGetRecord);

        return flushPromises()
          .then(() => {
            const btn = element.shadowRoot.querySelector(".apply-button");
            btn.click();

            return Promise.resolve();
          })
          .then(() => {
            let isLoading = element.shadowRoot.querySelector("c-advisr_modal")
              .loading;
            expect(isLoading).toBeTruthy();

            return flushPromises();
          })
          .then(() => {
            let isLoading = element.shadowRoot.querySelector("c-advisr_modal")
              .loading;
            expect(isLoading).toBeFalsy();
          });
      });
    });
  });
});
