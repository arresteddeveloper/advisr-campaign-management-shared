import { api } from 'lwc';
import Advisr_linkCampaignStep from "c/advisr_linkCampaignStep";

import Advisr_Link_Success_Message from "@salesforce/label/c.Advisr_Link_Success_Message";
import Advisr_Link_Success_Toast from "@salesforce/label/c.Advisr_Link_Success_Toast";

export default class Advisr_linkCampaignStepThree extends Advisr_linkCampaignStep {
  //Id of the selected campaign from the previous step
  @api campaignId;

  //Id of the opportunity that the campaign should be linked to
  @api opportunityId;

  //Boolean flag for showing either view or edit form for the campaign
  saveSuccess = false;

  //Custom labels
  Advisr_Link_Success_Message = Advisr_Link_Success_Message;

  @api submit() {
    //Submit the form using the provided submit button (hidden on the page)
    this.dispatchLoadingEvent(true);
    this.template.querySelector('lightning-button').click();
  }

  /**
   * Handle the save success event when the record edit form is successful
   */
  handleSuccess() {
    this.dispatchLoadingEvent(false);
    this.saveSuccess = true;
    this.template.querySelector("c-advisr_toast-message").show({
      variant: "success",
      message: Advisr_Link_Success_Toast
    });
  }
}
