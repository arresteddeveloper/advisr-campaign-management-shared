import { createElement } from "lwc";
import Advisr_UnlinkCampaignAction from "c/advisr_unlinkCampaignAction";
import {
  registerLdsTestWireAdapter,
  registerApexTestWireAdapter
} from "@salesforce/sfdx-lwc-jest";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import getAdvisrCampaignsById from "@salesforce/apex/advisr_UnlinkCampaignCtrl.getAdvisrCampaignsById";
import getRemainingActiveCampaigns from "@salesforce/apex/advisr_UnlinkCampaignCtrl.getRemainingActiveCampaigns";
import unlinkCampaigns from "@salesforce/apex/advisr_UnlinkCampaignCtrl.unlinkCampaigns";

// Realistic data with a list of advisr campaigns
const mockGetObjectInfo = require("./data/getObjectInfo.json");
const mockGetAdvisrCampaigns = require("./data/getAdvisrCampaigns.json");
const mockGetRemainingActiveCampaigns = require("./data/getRemainingActiveCampaigns.json");

// Register as Apex wire adapter. Some tests verify that provisioned values trigger desired behavior.
const getAdvisrCampaignsByIdAdapter = registerApexTestWireAdapter(
  getAdvisrCampaignsById
);
const getObjectInfoAdapter = registerLdsTestWireAdapter(getObjectInfo);

// Helper function to wait until the microtask queue is empty. This is needed for promise
// timing when calling createRecord.
function flushPromises() {
  // eslint-disable-next-line no-undef
  return new Promise((resolve) => setImmediate(resolve));
}

// Mocking imperative Apex method call
jest.mock(
  "@salesforce/apex/advisr_UnlinkCampaignCtrl.getRemainingActiveCampaigns",
  () => {
    return {
      default: jest.fn()
    };
  },
  { virtual: true }
);

jest.mock(
  "@salesforce/apex/advisr_UnlinkCampaignCtrl.unlinkCampaigns",
  () => {
    return {
      default: jest.fn()
    };
  },
  { virtual: true }
);

describe("c-advisr_-unlink-campaign-action", () => {
  afterEach(() => {
    // The jsdom instance is shared across test cases in a single file so reset the DOM
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }
    // Prevent data saved on mocks from leaking between tests
    jest.clearAllMocks();
  });

  const setup = (recordIds) => {
    getObjectInfoAdapter.emit(mockGetObjectInfo);
    const element = createElement("c-advisr_-unlink-campaign-action", {
      is: Advisr_UnlinkCampaignAction
    });
    element.recordIds = recordIds;
    document.body.appendChild(element);
    return element;
  };

  it("should throw an error when no campaign records are selected", () => {
    let element = setup();

    let errorMessage = element.shadowRoot.querySelector(".error-message");
    expect(errorMessage).not.toBeNull();
  });

  it("should show a list of selected campaign records if some are selected", () => {
    let element = setup(["1"]);

    // Assign mock value for resolved Apex promise
    getRemainingActiveCampaigns.mockResolvedValue(
      mockGetRemainingActiveCampaigns
    );

    // Emit data from @wire
    getAdvisrCampaignsByIdAdapter.emit(mockGetAdvisrCampaigns);

    return Promise.resolve().then(() => {
      let listItems = element.shadowRoot.querySelectorAll("ol > li");
      expect(listItems.length).toBe(2);
    });
  });

  it("should show a list of additional campaigns if removing the primary campaign and there are other campaigns on the opp", () => {
    let element = setup(["1"]);

    // Assign mock value for resolved Apex promise
    getRemainingActiveCampaigns.mockResolvedValue(
      mockGetRemainingActiveCampaigns
    );

    // Emit data from @wire
    getAdvisrCampaignsByIdAdapter.emit(mockGetAdvisrCampaigns);

    return flushPromises().then(() => {
      let dataTable = element.shadowRoot.querySelector("lightning-datatable");
      expect(dataTable).not.toBeNull();
      expect(dataTable.data).toHaveLength(2);
    });
  });

  it("should automatically set is_primary if there is only one undeleted remaining campaign", () => {
    let element = setup(["1"]);
    let onlyOneRemainingRecord = mockGetRemainingActiveCampaigns.slice(0, 1);

    let EXPECTED_INPUT = {
      campaignsToUnlink: mockGetAdvisrCampaigns,
      replacementPrimaryCampaign: onlyOneRemainingRecord[0]
    };

    // Assign mock value for resolved Apex promise
    getRemainingActiveCampaigns.mockResolvedValue(onlyOneRemainingRecord);
    unlinkCampaigns.mockResolvedValue(null);

    //Emit data from @wire
    getAdvisrCampaignsByIdAdapter.emit(mockGetAdvisrCampaigns);

    return Promise.resolve()
      .then(() => {
        let dataTable = element.shadowRoot.querySelector("lightning-datatable");
        expect(dataTable).toBeNull();

        let submitButton = element.shadowRoot.querySelector(".apply-button");
        submitButton.click();

        return Promise.resolve();
      })
      .then(() => {
        expect(unlinkCampaigns).toHaveBeenCalledTimes(1);
        expect(unlinkCampaigns.mock.calls[0][0]).toEqual(EXPECTED_INPUT);
      });
  });

  it("should allow users to select a replacement campaign when more than one campaign remains", () => {
    let element = setup(["1"]);

    let EXPECTED_INPUT = {
      campaignsToUnlink: mockGetAdvisrCampaigns,
      replacementPrimaryCampaign: mockGetRemainingActiveCampaigns[0]
    };

    // Assign mock value for resolved Apex promise
    getRemainingActiveCampaigns.mockResolvedValue(
      mockGetRemainingActiveCampaigns
    );
    unlinkCampaigns.mockResolvedValue(null);

    //Emit data from @wire
    getAdvisrCampaignsByIdAdapter.emit(mockGetAdvisrCampaigns);

    return flushPromises()
      .then(() => {
        let dataTable = element.shadowRoot.querySelector("lightning-datatable");
        expect(dataTable).not.toBeNull();

        dataTable.dispatchEvent(
          new CustomEvent("rowselection", {
            detail: {
              selectedRows: [mockGetRemainingActiveCampaigns[0]]
            }
          })
        );

        let submitButton = element.shadowRoot.querySelector(".apply-button");
        submitButton.click();

        return Promise.resolve();
      })
      .then(() => {
        expect(unlinkCampaigns).toHaveBeenCalledTimes(1);
        expect(unlinkCampaigns.mock.calls[0][0]).toEqual(EXPECTED_INPUT);
      });
  });

  it("should show an error toast when there is an error updating", () => {
    let element = setup(["1"]);

    let showToastHandler = jest.fn();
    let toast = element.shadowRoot.querySelector("c-advisr_toast-message");
    let toastSpy = jest.spyOn(toast, "show");
    element.addEventListener("advisr_showtoast", showToastHandler);

    // Assign mock value for resolved Apex promise
    getRemainingActiveCampaigns.mockResolvedValue(
      mockGetRemainingActiveCampaigns
    );
    unlinkCampaigns.mockRejectedValue(new Error("Forced Error"));

    //Emit data from @wire
    getAdvisrCampaignsByIdAdapter.emit(mockGetAdvisrCampaigns);

    return flushPromises()
      .then(() => {
        let dataTable = element.shadowRoot.querySelector("lightning-datatable");
        expect(dataTable).not.toBeNull();

        dataTable.dispatchEvent(
          new CustomEvent("rowselection", {
            detail: {
              selectedRows: [mockGetRemainingActiveCampaigns[0]]
            }
          })
        );

        let submitButton = element.shadowRoot.querySelector(".apply-button");
        submitButton.click();

        return flushPromises();
      })
      .then(() => {
        expect(toastSpy).toHaveBeenCalledTimes(1);
        expect(toastSpy).toHaveBeenCalledWith({
          variant: "error",
          message: "Forced Error"
        });
      });
  });

  it("should not show a list of additional campaigns if removing all campaigns from the opp", () => {
    let element = setup(["1"]);

    // Assign mock value for resolved Apex promise
    getRemainingActiveCampaigns.mockResolvedValue([]);

    //Emit data from @wire
    getAdvisrCampaignsByIdAdapter.emit(mockGetAdvisrCampaigns);

    flushPromises().then(() => {
      //There should be no selection required, so no table
      let dataTable = element.shadowRoot.querySelector("lightning-datatable");
      expect(dataTable).toBeNull();
    });
  });

  it("should not show a list of additional campaigns if not removing any primary campaigns", () => {
    let element = setup(["1"]);
    let mockGetAdvisrCampaignsClone = mockGetAdvisrCampaigns.map((campaign) => {
      let clone = JSON.parse(JSON.stringify(campaign));
      clone.Advisr_Campaign_Is_Primary__c = false;
      return clone;
    });

    //Emit data from @wire
    getAdvisrCampaignsByIdAdapter.emit(mockGetAdvisrCampaignsClone);

    flushPromises().then(() => {
      //There should be no selection required, so no table
      let dataTable = element.shadowRoot.querySelector("lightning-datatable");
      expect(dataTable).toBeNull();
    });
  });
});
