import getAdvisrCampaignsById from "@salesforce/apex/advisr_UnlinkCampaignCtrl.getAdvisrCampaignsById";
import getRemainingActiveCampaigns from "@salesforce/apex/advisr_UnlinkCampaignCtrl.getRemainingActiveCampaigns";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import { LightningElement, api, wire } from "lwc";
import { DatatableFieldBuilder, reduceErrors } from "c/advisr_utils";
import unlinkCampaigns from "@salesforce/apex/advisr_UnlinkCampaignCtrl.unlinkCampaigns";
import { NavigationMixin } from "lightning/navigation";

//Schema Imports
import ADVISR_CAMPAIGN_OBJECT from "@salesforce/schema/Advisr_Campaign__c";
import ADVISR_ID_FIELD from "@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Id__c";
import ADVISR_CAMPAIGN_NAME_FIELD from "@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Name__c";
import ADVISR_BUDGET_FIELD from "@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Budget__c";
import ADVISR_CREATED_DATE from "@salesforce/schema/Advisr_Campaign__c.CreatedDate";
import ADVISR_IS_PRIMARY_FIELD from "@salesforce/schema/Advisr_Campaign__c.Advisr_Campaign_Is_Primary__c";
import ADVISR_OPPORTUNITY_FIELD from "@salesforce/schema/Advisr_Campaign__c.Opportunity__c";

//Label imports
import Advisr_Unlink_Success_Message from "@salesforce/label/c.Advisr_Unlink_Success_Message";
import Advisr_Unlink_Success_Toast from "@salesforce/label/c.Advisr_Unlink_Success_Toast";
import Advisr_Unlink_Campaign_Component_Label from "@salesforce/label/c.Advisr_Unlink_Campaign_Component_Label";
import Advisr_Unlink_No_Records_Selected from "@salesforce/label/c.Advisr_Unlink_No_Records_Selected";
import Advisr_Unlink_Campaign_Confirmation_Message from "@salesforce/label/c.Advisr_Unlink_Campaign_Confirmation_Message";
import Advisr_Unlink_Campaign_New_Primary_Required from "@salesforce/label/c.Advisr_Unlink_Campaign_New_Primary_Required";

export default class Advisr_UnlinkCampaignAction extends NavigationMixin(
  LightningElement
) {
  //List of selected recordIds that are being unlinked
  @api recordIds;

  //Related Opportunity ID used to navigate back to related list page
  @api opportunityId;

  //Optional comfigurable page title
  @api pageTitle = Advisr_Unlink_Campaign_Component_Label;

  @wire(getAdvisrCampaignsById, {
    campaignIds: "$recordIds"
  })
  wiredSelectedCampaignRecords({ data, error }) {
    if (!data) return;

    this.selectedCampaignRecords = data;
    this.validateIfPrimarySelected();

    //If the primary record is not selected, no need to do anything else
    if (!this.isPrimarySelected) return;

    //Query out the remaining active campaigns if there are any
    getRemainingActiveCampaigns({
      unlinkingCampaignIds: this.recordIds,
      opportunityId: this.selectedCampaignRecords[0][
        ADVISR_OPPORTUNITY_FIELD.fieldApiName
      ]
    })
      .then((results) => {
        this.remainingCampaignRecords = results;
        this.validateIfSelectionRequired();
      })
      .catch((error) => {
        window.console.log(error);
      });
  }

  @wire(getObjectInfo, {
    objectApiName: ADVISR_CAMPAIGN_OBJECT
  })
  wiredObjectInfo({ data, error }) {
    if (data) {
      let fieldBuilder = new DatatableFieldBuilder(data);
      this.remainingRecordsColumns = fieldBuilder.build([
        ADVISR_ID_FIELD,
        ADVISR_CAMPAIGN_NAME_FIELD,
        ADVISR_BUDGET_FIELD,
        ADVISR_CREATED_DATE
      ]);
    }
  }

  //List of columns used in the UI
  remainingRecordsColumns = [];

  //Lists of records
  selectedCampaignRecords = [];
  remainingCampaignRecords = [];

  //Reference variables
  opportunityId;

  //UI validation checks for conditional rendering
  isPrimarySelected;
  isSelectionRequired;
  saveComplete;
  saveErrors;

  //Replacement record to be used in the case where there needs to be a primary campaign
  selectedPrimaryCampaign;

  //Loading indicator
  loading = false;

  //Custom labels
  Advisr_Unlink_Success_Message = Advisr_Unlink_Success_Message;
  Advisr_Unlink_Campaign_Confirmation_Message = Advisr_Unlink_Campaign_Confirmation_Message;
  Advisr_Unlink_Campaign_New_Primary_Required = Advisr_Unlink_Campaign_New_Primary_Required;

  //**************************************************
  // Internal methods
  // */

  //Validate if any of the selected records are primary
  validateIfPrimarySelected() {
    let isPrimarySelected = this.selectedCampaignRecords.reduce(
      (previousValue, currentValue) => {
        if (currentValue[ADVISR_IS_PRIMARY_FIELD.fieldApiName] === true)
          previousValue = true;
        return previousValue;
      },
      false
    );

    this.isPrimarySelected = isPrimarySelected;
  }

  //Validate if the user is required to select a primary campaign to replace an unlinked one
  validateIfSelectionRequired() {
    //If there is only one remaining record, no selection required
    //If there is more than one remaining record, selection is required
    //One edge case is if the user has a selected primary campaign and there is one remaining primary campaign that is covered for here
    let isPrimaryCampaignRemaining = this.remainingCampaignRecords.reduce(
      (previousValue, currentValue) => {
        if (currentValue[ADVISR_IS_PRIMARY_FIELD.fieldApiName] === true)
          previousValue = true;
        return previousValue;
      },
      false
    );

    this.isSelectionRequired =
      this.remainingCampaignRecords.length > 1 && !isPrimaryCampaignRemaining;

    //If the user doesn't need to make a selection
    //And there is only one remaining record, use that record (if it isn't already primary)
    if (!this.isSelectionRequired && !isPrimaryCampaignRemaining) {
      this.selectedPrimaryCampaign = this.remainingCampaignRecords[0];
    }
  }

  //**************************************************
  // ACTIONS
  //*/

  //Return to the previous page
  cancel() {
    window.location.assign(
      "/lightning/r/Opportunity/" + this.opportunityId + "/view"
    );
  }

  /**
   * Apply the save. First ensure that at least one record is selected, and if not, then
   */
  apply() {
    if (this.isSelectionRequired && !this.selectedPrimaryCampaign) {
      this.template.querySelector('c-advisr_toast-message').show({
        variant: "error",
        message: Advisr_Unlink_Campaign_New_Primary_Required
      })
      return;
    }
    this.loading = true;
    unlinkCampaigns({
      campaignsToUnlink: this.selectedCampaignRecords,
      replacementPrimaryCampaign: this.selectedPrimaryCampaign
    })
      .then(() => {
        this.template.querySelector("c-advisr_toast-message").show({
          variant: "success",
          message: Advisr_Unlink_Success_Toast
        });
        this.saveComplete = true;
      })
      .catch((err) => {
        this.template.querySelector("c-advisr_toast-message").show({
          variant: "error",
          message: reduceErrors(err)[0]
        });
        this.saveErrors = reduceErrors[0];
      })
      .finally(() => {
        this.loading = false;
      });
  }

  //**************************************************
  // Event handlers
  //*/

  /**
   * Callback event for when the user makes a selection from the lightning-datatable
   */
  handleCampaignSelection(event) {
    const selectedRows = event.detail.selectedRows;
    this.selectedPrimaryCampaign = selectedRows[0];
  }

  //**************************************************
  // Dynamic parameters
  //*/

  //Retrieve the error message if there is any
  //These are the following scenarios that may throw an error
  //2. No campaigns have been selected
  //4. There was an error loading the records
  //5. There was an error updating/saving the records
  get errorMessage() {
    if (!this.recordIds || this.recordIds.length === 0)
      return Advisr_Unlink_No_Records_Selected;
    if (!!this.saveErrors) return this.saveErrors;
    return null;
  }

  get showRecords() {
    return !this.errorMessage && !!this.selectedCampaignRecords;
  }

  get showOKButton() {
    return this.saveComplete || !!this.errorMessage;
  }

  get showCancelButton() {
    return !this.showOKButton;
  }
}
