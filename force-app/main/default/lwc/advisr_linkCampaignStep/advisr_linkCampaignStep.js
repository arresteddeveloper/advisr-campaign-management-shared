import { getObjectInfos } from "lightning/uiObjectInfoApi";
import { LightningElement, wire, api } from "lwc";

import ADVISR_CAMPAIGN_OBJECT from '@salesforce/schema/Advisr_Campaign__c';
import ADVISR_CLIENT_OBJECT from '@salesforce/schema/Advisr_Client__c';

export default class Advisr_linkCampaignStep extends LightningElement {
  //This method should be overridden
  @api validateStep() {
    return true;
  }

  //This method should also be overridden
  @api getStepData() {
    return {}
  }
  
  //Retrieve the describe information for the objects we are interested in
  //This allows a single call instead of a separate call for each
  //Retrieve specific describe information from the getObjectInfo method
  //NOTE: This is an asynchronous method and data may not be available immediately upon load
  @wire(getObjectInfos, { objectApiNames: [ ADVISR_CAMPAIGN_OBJECT, ADVISR_CLIENT_OBJECT ]})
  objectInfos({ data, error }) {
    if (!data) return;

    for (let result of data.results) {
      if (result.statusCode === 200)
        this.objectInfoMap[result.result.apiName] = result.result;
    }
  }

  //Store the describe info on the class for easy reference by inherited components
  ADVISR_CAMPAIGN_OBJECT = ADVISR_CAMPAIGN_OBJECT;
  ADVISR_CLIENT_OBJECT = ADVISR_CLIENT_OBJECT;

  //A map of describe information for reference in inherited components
  objectInfoMap = {};

  /**
   * Retrieve the specific Object info required from the getObjectInfos wired method
   * @param {Object} object LWC describe import from the following '@salesforce/schema/OBJECT_NAME'
   */
  getObjectInfo(object) {
    return this.objectInfoMap[object.objectApiName];
  }

  /**
   * Fire an even whenever the component is performing an action that requires loading
   * @param {Boolean} isLoading Whether or not the caller is loading
   */
  dispatchLoadingEvent(isLoading = true) {
    this.dispatchEvent(new CustomEvent('advisrloadingchange', {
      detail: {
        isLoading
      }
    }));
  }
}
