import { LightningElement, api } from 'lwc';

export default class Placeholder extends LightningElement {
    @api title;
    @api iconName = 'standard:action_list_component';
}