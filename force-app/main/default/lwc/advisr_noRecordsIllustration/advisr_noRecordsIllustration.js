import { LightningElement, api } from 'lwc';

export default class NoRecordsIllustration extends LightningElement {
  @api message = "";
  @api size = "small"

  get sizeClass() {
    return `slds-illustration slds-illustration_${this.size}`;
  }
}