import { api, wire } from "lwc";
import Advisr_linkCampaignStep from "c/advisr_linkCampaignStep";
import { reduceErrors, DatatableFieldBuilder } from "c/advisr_utils";

//Apex imports
import searchClients from "@salesforce/apex/advisr_LinkCampaignCtrl.searchClients";
import searchGroups from "@salesforce/apex/advisr_LinkCampaignCtrl.searchGroups";
import getRecentlyViewedGroups from "@salesforce/apex/advisr_LinkCampaignCtrl.getRecentlyViewedGroups";

//Schema imports
import ADVISR_CLIENT_NAME_FIELD from "@salesforce/schema/Advisr_Client__c.Advisr_Client_Name__c";
import ADVISR_CLIENT_ID_FIELD from "@salesforce/schema/Advisr_Client__c.Advisr_Client_Id__c";
import ADVISR_CLIENT_GROUP_FIELD from "@salesforce/schema/Advisr_Client__c.Advisr_Group_Name__c";
import ADVISR_CLIENT_CREATED_DATE_FIELD from "@salesforce/schema/Advisr_Client__c.CreatedDate";

const DEBOUNCE_DELAY = 350;
export default class Advisr_linkCampaignStepOne extends Advisr_linkCampaignStep {
  //External API
  @api opportunityId;

  //The component should only be valid when a client has been selected
  @api validateStep() {
    return !!this.clientId;
  }

  @api getStepData() {
    return {
      clientId: this.clientId
    }
  }

  //Loading indicator
  loading = false;

  //List of clients based on the search criteria
  clientList = [];

  //Timeout function stored for clearTimeout functionality
  delayTimeout;

  //Default search results based on recently viewed group records
  recentlyViewedGroups;

  /**
   * Loads recently viewed records and set them as default lookpup search results (optional)
   */
  @wire(getRecentlyViewedGroups)
  getRecentlyViewed({ data, error }) {
    if (data) {
      this.recentlyViewedGroups = data;
      this.initLookupDefaultResults();
    }
  }

  /****************************************************
   * Actions
   */

  //Event handler for when the input changes
  handleInputChange() {
    window.clearTimeout(this.delayTimeout);
    // eslint-disable-next-line @lwc/lwc/no-async-operation
    this.delayTimeout = setTimeout(() => {
      this.search();
    }, DEBOUNCE_DELAY);
  }

  //Event handler for when the group lookup changes
  handleSelectionChange() {
    this.search();
  }

  handleGroupSearch(event) {
    const lookupElement = event.target;

    // Call Apex endpoint to search for records and pass results to the lookup
    searchGroups(event.detail)
      .then((results) => {
        lookupElement.setSearchResults(results);
      })
      .catch((error) => {
        this.throwError(
          "An error occured while searching with the lookup field."
        );
        // eslint-disable-next-line no-console
        console.error("Lookup error", JSON.stringify(error));
      });
  }

  /****************************************************
   * Internal helper methods
   */

  /**
   * Initializes the lookup default results with a list of recently viewed records (optional)
   */
  initLookupDefaultResults() {
    // Make sure that the lookup is present and if so, set its default results
    const lookup = this.template.querySelector("c-advisr_lookup");
    if (lookup) {
      lookup.setDefaultResults(this.recentlyViewedGroups);
    }
  }

  //Perform a search of the database for Advisr_Client__c records that match the search criteria
  search() {
    let searchInput = this.template.querySelector("lightning-input");
    let searchTerm = searchInput.value || null;

    let groupInput = this.template.querySelector("c-advisr_lookup");
    let groupValue = groupInput.selection || null;

    //The user must have at least a search term or a group selected
    if (
      (!searchTerm) &&
      (!groupValue || groupValue.length === 0)
    ) {
      return;
    }

    //Retrieve the selected groupId filter
    let groupId =
      (!!groupValue && groupValue.length) === 1 ? groupValue[0].id : null;

    //Flag the component as loading and do a search for client records
    this.loading = true;
    this.clientList = [];
    searchClients({
      searchTerm,
      groupId,
      opportunityId: this.opportunityId
    })
      .then((results) => {
        this.clientList = results.map((result) => {
          let clone = JSON.parse(JSON.stringify(result));
          clone.campaignCount = result[this.getAdvisrCampaignRelationshipName()]
            ? result[this.getAdvisrCampaignRelationshipName()].length
            : 0;
          return clone;
        });
      })
      .catch((err) => {
        this.throwError(reduceErrors(err)[0]);
      })
      .finally(() => {
        this.loading = false;
      });
  }

  //Retrieve the metadata for the Advisr_Client__c object
  getClientInfoObject() {
    return this.getObjectInfo(this.ADVISR_CLIENT_OBJECT);
  }

  getAdvisrCampaignRelationshipName() {
    let obj = this.getClientInfoObject();
    if (!obj) return 'Advisr_Campaigns__r';

    return obj.childRelationships
      .find(relationship => relationship.childObjectApiName === this.ADVISR_CAMPAIGN_OBJECT.objectApiName)
      .relationshipName;
  }

  //Thow an error message to the user
  throwError(message = "Search terms must be greater than 3 characters") {
    this.template.querySelector("c-advisr_toast-message").show({
      message,
      variant: "error"
    });
  }

  /****************************************************
   * Dynamic properties
   */
  get tableColumns() {
    if (!this.getClientInfoObject()) return [];

    let clientObject = this.getClientInfoObject();
    let builder = new DatatableFieldBuilder(clientObject);
    return builder.build([
      ADVISR_CLIENT_ID_FIELD,
      ADVISR_CLIENT_NAME_FIELD,
      ADVISR_CLIENT_GROUP_FIELD,
      {
        label: "Campaigns",
        fieldName: "campaignCount",
        type: "number",
        isCustom: true
      },
      ADVISR_CLIENT_CREATED_DATE_FIELD
    ]);
  }

  get clientId() {
    let dataTable = this.template.querySelector("lightning-datatable");
    if (!dataTable) return null;

    let selectedRows = dataTable.getSelectedRows();
    if (!selectedRows || selectedRows.length === 0) return null;


    return selectedRows[0].Id;
  }

  get noData() {
    return this.clientList && this.clientList.length === 0 && !this.loading;
  }

  get noDataMessage() {
    return "Please make sure you select at least one filter from the above criteria and don't forget to click search";
  }
}
