import { createElement } from 'lwc';
import Advisr_linkCampaignStepOne from 'c/advisr_linkCampaignStepOne';
import searchClients from '@salesforce/apex/advisr_LinkCampaignCtrl.searchClients';

//Mock data
import SEARCH_CLIENTS_SUCCESS from './data/searchClients.json';
import GET_CLIENT_INFO_OBJECT from './data/getClientInfo.json';

// Mocking imperative Apex method call
jest.mock(
    '@salesforce/apex/advisr_LinkCampaignCtrl.searchClients',
    () => {
        return {
            default: jest.fn()
        };
    },
    { virtual: true }
);

jest.useFakeTimers();

describe('c-advisr_link-campaign-step-one', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }

        // Prevent data saved on mocks from leaking between tests
        jest.clearAllMocks();
    });

    // Helper function to wait until the microtask queue is empty. This is needed for promise
    // timing when calling imperative Apex.
    function flushPromises() {
        // eslint-disable-next-line no-undef
        return new Promise((resolve) => setImmediate(resolve));
    }

    function setup() {
        let element = createElement('c-advisr_link-campaign-step-one', {
            is: Advisr_linkCampaignStepOne
        });
        document.body.appendChild(element);
        return element;
    }

    describe('Search functionality', () => {
        it('passes the user input to the Apex method correctly', () => {
            const USER_INPUT = 'Hello';
            const APEX_PARAMETERS = { searchTerm: USER_INPUT, groupId: null };
            
            searchClients.mockResolvedValue(SEARCH_CLIENTS_SUCCESS);
            
            
            let element = setup();
            element.shadowRoot.querySelector('lightning-input').value = 'Hello';
            let input = element.shadowRoot.querySelector('lightning-input');
            input.value = USER_INPUT;
            input.dispatchEvent(new CustomEvent('change'));
    
            // Fast-forward until all timers have been executed
            jest.runAllTimers();
    
            return flushPromises().then(() => {
                expect(searchClients.mock.calls[0][0]).toEqual(APEX_PARAMETERS);
            })
        })
    
        it('should allow both a search input and a group input', () => {
            const USER_INPUT = 'Hello';
            const APEX_PARAMETERS = { searchTerm: USER_INPUT, groupId: '1' };
            
            searchClients.mockResolvedValue(SEARCH_CLIENTS_SUCCESS);
            
            let element = setup();
            element.shadowRoot.querySelector('lightning-input').value = 'Hello';
            let input = element.shadowRoot.querySelector('lightning-input');
            input.value = USER_INPUT;
            input.dispatchEvent(new CustomEvent('change'));

            let groupInput = element.shadowRoot.querySelector('c-advisr_lookup');
            groupInput.selection = [{ id: '1', title: 'Hello World'}];
            groupInput.dispatchEvent(new CustomEvent('selectionchange'))

            jest.runAllTimers();

            return flushPromises().then(() => {
                expect(searchClients.mock.calls[0][0]).toEqual(APEX_PARAMETERS);
            })
        })
    
        it('should not allow users to search unless there is no search', () => {
            const USER_INPUT = '';
            
            searchClients.mockResolvedValue(SEARCH_CLIENTS_SUCCESS);
            
            let element = setup();
            let input = element.shadowRoot.querySelector('lightning-input');
            input.value = USER_INPUT;
            input.dispatchEvent(new CustomEvent('change'));
    
            // Fast-forward until all timers have been executed
            jest.runAllTimers();
    
            return flushPromises().then(() => {
                expect(searchClients).not.toHaveBeenCalled();
            })
        })
    
        it('should start with no group filter', () => {
            let element = setup();
    
            let groupInput = element.shadowRoot.querySelector('c-advisr_lookup');
    
            expect(groupInput.selection).toEqual([]);
        })
    
        it('should allow users to filter the data by group', () => {
            const APEX_PARAMETERS = { searchTerm: null, groupId: '1' };
            
            searchClients.mockResolvedValue(SEARCH_CLIENTS_SUCCESS);
            
            let element = setup();
            let groupInput = element.shadowRoot.querySelector('c-advisr_lookup');
            groupInput.selection = [{ id: '1', title: 'Hello World'}];
            groupInput.dispatchEvent(new CustomEvent('selectionchange'))
    
            // Fast-forward until all timers have been executed
            jest.runAllTimers();
    
            return flushPromises().then(() => {
                expect(searchClients.mock.calls[0][0]).toEqual(APEX_PARAMETERS);
            })
        })
    })

    describe('Data display', () => {
        it('should show a notification when there is no data to show', () => {
            let element = setup();
            
            let message = element.shadowRoot.querySelector('c-advisr_no-records-illustration');
            expect(message).not.toBeNull();
    
            const USER_INPUT = 'Hello';
            
            searchClients.mockResolvedValue([]);
            
            element.shadowRoot.querySelector('lightning-input').value = 'Hello';
            let input = element.shadowRoot.querySelector('lightning-input');
            input.value = USER_INPUT;
            input.dispatchEvent(new CustomEvent('change'));
    
            // Fast-forward until all timers have been executed
            jest.runAllTimers();
    
            return flushPromises().then(() => {
                expect(message).not.toBeNull();
            })
        })
    })

    describe('External API', () => {
        it('should validate to false when improperly configured', () => {
            let element = setup();
    
            expect(element.validateStep()).toBe(false);
        })
    
        it('should validate to true when properly configured', () => {
            const USER_INPUT = 'Hello';
            const APEX_PARAMETERS = { searchTerm: USER_INPUT };
            
            searchClients.mockResolvedValue(SEARCH_CLIENTS_SUCCESS);
            
            let element = setup();
            element.shadowRoot.querySelector('lightning-input').value = 'Hello';
            let input = element.shadowRoot.querySelector('lightning-input');
            input.value = USER_INPUT;
            input.dispatchEvent(new CustomEvent('change'));
    
            // Fast-forward until all timers have been executed
            jest.runAllTimers();
    
            return flushPromises().then(() => {
                let table = element.shadowRoot.querySelector('lightning-datatable');
                table.selectedRows = [{"Id":"a010R00000AwrqQQAR","Advisr_Client_Id__c":"123","Advisr_Client_Name__c":"Hello World","Advisr_Group_Name__c":"Group A","CreatedDate":"2021-02-02T21:31:49.000Z","campaignCount":0}];
                
                return Promise.resolve()
            }).then(() => {
                expect(element.validateStep()).toBe(true);
            })
        })

        it('should provide selected information in key => value format', () => {
            const USER_INPUT = 'Hello';
            const APEX_PARAMETERS = { searchTerm: USER_INPUT };
            
            searchClients.mockResolvedValue(SEARCH_CLIENTS_SUCCESS);
            
            let element = setup();
            element.shadowRoot.querySelector('lightning-input').value = 'Hello';
            let input = element.shadowRoot.querySelector('lightning-input');
            input.value = USER_INPUT;
            input.dispatchEvent(new CustomEvent('change'));
    
            // Fast-forward until all timers have been executed
            jest.runAllTimers();
    
            return flushPromises().then(() => {
                let table = element.shadowRoot.querySelector('lightning-datatable');
                table.selectedRows = [{"Id":"a010R00000AwrqQQAR","Advisr_Client_Id__c":"123","Advisr_Client_Name__c":"Hello World","Advisr_Group_Name__c":"Group A","CreatedDate":"2021-02-02T21:31:49.000Z","campaignCount":0}];
                
                return Promise.resolve()
            }).then(() => {
                expect(element.getStepData()).toEqual({
                    clientId: "a010R00000AwrqQQAR"
                });
            })
        })
    })
});