/*
 * Name          : advisr_GroupSelector
 * Author        : Shift CRM
 * Description   : Apex class used to handle queries on the Advisr_Group__c object
 * TODO: May look to expand this in the future and create a more generic SObject Selector class that this can extend. Not necessary for now
 *
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
public class advisr_GroupSelector extends advisr_SObjectSelector {
  /**
   * List of fields to be used in most query cases
   */
  private List<Schema.SObjectField> getSObjectFieldList() {
    return new List<Schema.SObjectField>{
      Advisr_Group__c.Id,
      Advisr_Group__c.Name,
      Advisr_Group__c.Advisr_Group_Name__c
    };
  }

  public Schema.SObjectType getSObjectType() {
    return Advisr_Group__c.SObjectType;
  }

  /**
   * Retrieve 10 most Recently Viewed Advisr_Group__c records
   * NOTE: Ignores security
   */
  public List<Advisr_Group__c> getRecentlyViewedGroups() {
    List<RecentlyViewed> recentlyViewed = (List<RecentlyViewed>) Database.query(
      this.getRecentlyViewed()
        .setLimit(10)
        .toSOQL()
    );

    List<Id> recentlyViewedIds = new List<Id>();
    for (RecentlyViewed viewed : recentlyViewed) {
      recentlyViewedIds.add(viewed.Id);
    }

    //Return the actual group records from the recently viewed
    return this.selectSObjectsById(recentlyViewedIds);
  }

  /**
   * Retrieve Advisr_Group__c records by searching on the name field
   * NOTE: Ignores security
   * @param searchTerm String value to be used to search on the Name field
   */
  public List<Advisr_Group__c> searchAdvisrGroupsByName(String searchTerm) {
    //Add wildcards to the searchTerm
    searchTerm = '%' + searchTerm + '%';

    return (List<Advisr_Group__c>) Database.query(
      newQueryFactory()
        .setCondition('Advisr_Group_Name__c LIKE :searchTerm')
        .setLimit(20)
        .toSOQL()
    );
  }
}
