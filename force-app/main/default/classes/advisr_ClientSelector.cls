/*
 * Name          : advisr_ClientSelector
 * Author        : Shift CRM
 * Description   : Apex class used to handle queries on the Advisr_Client__c object
 *
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
public class advisr_ClientSelector extends advisr_SObjectSelector {
  /**
   * List of fields to be used in most query cases
   */
  private List<Schema.SObjectField> getSObjectFieldList() {
    return new List<Schema.SObjectField>{
      Advisr_Client__c.Id,
      Advisr_Client__c.Advisr_Client_Id__c,
      Advisr_Client__c.Advisr_Client_Name__c,
      Advisr_Client__c.Advisr_Group_Name__c,
      Advisr_Client__c.CreatedDate
    };
  }

  public Schema.SObjectType getSObjectType() {
    return advisr_Client__c.SObjectType;
  }

  /**
   * Retrieve All Advisr_Campaign__c records by ID that have related campaign records
   * NOTE: Ignores security
   * @param searchTerm String value to search for on the Advisr_Client_Name__c field
   * @param groupId Optional groupId to additionally filter by on the Advisr_Group__c field
   * @param opportunityId Used to filter out the child campaign records that are queried as pa
   */
  public List<Advisr_Client__c> searchAdvisrClientsByNameAndGroup(
    String searchTerm, 
    Id groupId, 
    String opportunityId
  ) {
    //Add wildcards to the searchTerm
    if (searchTerm != null) searchTerm = '%' + searchTerm + '%';

    //Query both the client, and the related advisr campaign records related to the client
    advisr_QueryFactory clientFactory = newQueryFactory();
    advisr_QueryFactory campaignFactory = new advisr_CampaignSelector()
      .addQueryFactorySubselect(clientFactory)
      .setCondition('Advisr_Campaign_Is_Deleted__c = false AND Opportunity__c != :opportunityId');
    
    String condition = searchTerm != null && groupId == null ? 'Advisr_Client_Name__c LIKE :searchTerm' :
      searchTerm != null && groupId != null ? 'Advisr_Client_Name__c LIKE :searchTerm AND Advisr_Group__c = :groupId' :
      searchTerm == null && groupId != null ? 'Advisr_Group__c = :groupId' :
      '';
    
    condition += ' AND Id IN (SELECT Advisr_Client__c FROM Advisr_Campaign__c WHERE Advisr_Campaign_Is_Deleted__c = false AND Opportunity__c != :opportunityId)';
    return (List<Advisr_Client__c>) Database.query(
      clientFactory
        .setCondition(condition)
        .setLimit(100)
        .toSOQL()
    );
  }
}
