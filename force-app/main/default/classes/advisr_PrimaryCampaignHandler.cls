/*
* Name          : advisr_PrimaryCampaignHandler
* Author        : Shift CRM
* Description   : Apex class to handle automated logic related to managing the Primary Campaign
*
* Maintenance History:
* Date ------------ Name  ----  Version --- Remarks
* 1/29/2021     Ryan Morden       0.1       Initial
*/
public class advisr_PrimaryCampaignHandler {

    public Map<Id, advisr_Campaign__c> errorMap = new Map<Id, advisr_Campaign__c>(); //Error management map

    public static void routeLogic (List<advisr_Campaign__c> campaignLst, Map<Id, advisr_Campaign__c> campaignOldMap ) { 

        Set<Id> OpportunityIdSet = new Set<Id>();
        Set<Id> OpportunityIdReset = new Set<Id>();
        Set<Id> PrimaryCampaignIdSet = new Set<Id>();
        Set<Id> OpportunityIdDefaultSet = new Set<Id>();
        Map<Id, advisr_Campaign__c> CampaignToDefaultMap = new Map<Id, advisr_Campaign__c>();

        for (advisr_Campaign__c campaign :campaignLst) {

            advisr_Campaign__c campaignOld; 
            if (!campaignOldMap.isEmpty()) campaignOld = campaignOldMap.get(campaign.Id); 

            //Only process primary records
            if (campaign.Advisr_Campaign_Is_Primary__c==true) {

                //Campaign has been flagged as Primary 
                if (campaignOld==NULL || (campaignOld!=NULL && campaignOld.Advisr_Campaign_Is_Primary__c==false)) {
                    //Mark any existing Primaries for this opportunite to false
                    if (campaign.Opportunity__c!=null) {
                        OpportunityIdReset.add(campaign.Opportunity__c);
                        PrimaryCampaignIdSet.add(campaign.Id);
                    }
                } 
                //Campaign Marked as deleted
                if (campaign.Advisr_Campaign_Is_Deleted__c==true) { 
                    campaign.Advisr_Campaign_Is_Primary__c=false; //set to false
                    //Query out all related to this and set the newsest to primary
                    if (campaign.Opportunity__c!=null) OpportunityIdSet.add(campaign.Opportunity__c);
                } 
                //Reparented to new opportunity
                if (campaignOld!=NULL && campaign.Opportunity__c!=campaignOld.Opportunity__c) { 
                    //Query out all related to this and set the newsest to primary for new and old opp links
                    if (campaign.Opportunity__c!=null) { //relink
                        OpportunityIdReset.add(campaign.Opportunity__c);
                        PrimaryCampaignIdSet.add(campaign.Id);
                        if (campaignOld.Opportunity__c!=null) OpportunityIdSet.add(campaignOld.Opportunity__c);
                    } 
                } 

            } else {
                //check if there is a existing primary, else make this first campaign the primary
                if ( (campaignOld==NULL && campaign.Opportunity__c!=null)) {
                    if (campaign.Opportunity__c!=null) {
                        OpportunityIdSet.add(campaign.Opportunity__c);
                        CampaignToDefaultMap.put(campaign.Opportunity__c, campaign);
                    }    
                }
            }
        }

        if (!CampaignToDefaultMap.isEmpty()) defaultInitialPrimary(CampaignToDefaultMap);
        if (!OpportunityIdSet.isEmpty() || !OpportunityIdReset.isEmpty() ) resetNewPrimary(OpportunityIdSet, OpportunityIdReset, PrimaryCampaignIdSet);
        
    }
 
    //Set the first Campaign as default
    public static void defaultInitialPrimary(Map<Id, advisr_Campaign__c> CampaignToDefaultMap) {   
        Set<Id> OppsWithPrimary = new Set<Id>();

        for (advisr_Campaign__c campaign :[SELECT ID, Opportunity__c FROM advisr_Campaign__c 
                                                WHERE Opportunity__c IN:CampaignToDefaultMap.KeySet() AND Advisr_Campaign_Is_Primary__c=TRUE]) { 
            OppsWithPrimary.add(campaign.Opportunity__c);                                        
        }                                            
        
        for (Id oppId :CampaignToDefaultMap.KeySet()) {
            if (!OppsWithPrimary.contains(oppId)) {
                CampaignToDefaultMap.get(oppId).Advisr_Campaign_Is_Primary__c=true;
            }
        }

    }    

    //Get most recent non deleted Campaign, mark as primary
    public static void resetNewPrimary(Set<Id> OpportunityIdSet, Set<Id> OpportunityIdReset, Set<Id> PrimaryCampaignIdSet) { 
        List<advisr_Campaign__c> advisrCampaignUpdateLst = new List<advisr_Campaign__c>();
        Set<Id> opportunitiesProcessed = new Set<Id>(); 

        for (advisr_Campaign__c campaign :[SELECT ID, Opportunity__c, Advisr_Campaign_Is_Primary__c FROM advisr_Campaign__c 
                                                WHERE (Opportunity__c IN:OpportunityIdSet OR Opportunity__c IN:OpportunityIdReset) ORDER BY CreatedDate desc]) { 
                                                                                                                                            
            if (OpportunityIdSet.contains(campaign.Opportunity__c)) { //Set new primary
                if (!opportunitiesProcessed.contains(campaign.Opportunity__c) && !PrimaryCampaignIdSet.contains(campaign.Id) && campaign.Advisr_Campaign_Is_Primary__c==false) {
                    campaign.Advisr_Campaign_Is_Primary__c = true;
                    advisrCampaignUpdateLst.add(campaign);
                    opportunitiesProcessed.add(campaign.Opportunity__c);
                }
            } else if (OpportunityIdReset.contains(campaign.Opportunity__c) && !PrimaryCampaignIdSet.contains(campaign.Id) && campaign.Advisr_Campaign_Is_Primary__c==true) { //Reset Primary
                campaign.Advisr_Campaign_Is_Primary__c = false;
                advisrCampaignUpdateLst.add(campaign);
            }
        
        }

        //Update results
        Database.SaveResult[] srList = Database.update(advisrCampaignUpdateLst, false);
        System.debug('Database.SaveResult advisrCampaignUpdateLst: ' + advisrCampaignUpdateLst);
        Integer counter = 0;
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                 for(Database.Error err : sr.getErrors()) {
                    System.debug('SFID=' + sr.getId() + ' - ' + err.getStatusCode() + ': ' + err.getMessage());
                    if (trigger.newmap.containskey(sr.getId())) {
                        trigger.newmap.get(sr.getId()).addError( err.getMessage() );
                    }
                }
            }
            counter++;
        }      

    }
}