/*
 * Name          : advisr_LinkCampaignCtrl
 * Author        : Shift CRM
 * Description   : Apex class used by the advisr_LinkCampaign LWC
 *
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
public with sharing class advisr_LinkCampaignCtrl {

  /**
   * Search the Advisr_Client__c table on the name field for matches based on the provided search term
   * @param searchTerm String value used to search the Advisr_Client__c table
   * @param groupId Id of an Advisr_Group__c record used for further or alternative filtering
   * @param opportunityId Id of an Opportunity used for excluding campaigns from the same opportunity in the client subquery
   */
  @AuraEnabled(Cacheable=true)
  public static List<Advisr_Client__c> searchClients(
    String searchTerm,
    Id groupId,
    String opportunityId
  ) {
    advisr_ClientSelector selector = new advisr_ClientSelector();
    return selector.searchAdvisrClientsByNameAndGroup(
      searchTerm,
      groupId,
      opportunityId
    );
  }

  /**
   * Search through the Advisr_Campaign__c table and search for campaigns by client
   * @param searchTerm String value used to search for specific campaigns by name
   * @param clientId Id of an Advisr_Client__c record to be used for filtering the list of campaigns
   * @param opportunityId Id of an Opportunity record used for showing records from different opportunities
   */
  @AuraEnabled(cacheable=true)
  public static List<Advisr_Campaign__c> searchAdvisrClientCampaigns(
    String searchTerm,
    Id clientId,
    Id opportunityId
  ) {
    advisr_CampaignSelector selector = new advisr_CampaignSelector();
    return selector.searchCampaignsByNameAndClient(
      searchTerm,
      clientId,
      opportunityId
    );
  }

  /**
   * Retrieve a list of recentl viewed groups for when the user does not provide a default search value
   */
  @AuraEnabled(cacheable=true)
  public static List<advisr_LookupSearchResult> getRecentlyViewedGroups(){
    List<advisr_LookupSearchResult> results = new List<advisr_LookupSearchResult>();
    // Get recently viewed group records
    advisr_GroupSelector selector = new advisr_GroupSelector();
    List<Advisr_Group__c> groupRecords = selector.getRecentlyViewedGroups();
    for (Advisr_Group__c record : groupRecords) {
      results.add(new advisr_LookupSearchResult(
        record.Id,
        'Advisr_Group__c',
        'standard:action_list_component',
        record.Advisr_Group_Name__c,
        ''
      ));
    }

    return results;
  }

  /**
   * Method for searching through the Advisr_Group__c table and returning the values in a LookupSearchResult array
   * @param searchTerm String value to search for in the Advisr_Group__c table
   * @param selectedIds Optional list of IDs that have already been selected by the user (this is not used currently and only used for multi-select)
   */
  @AuraEnabled
  public static List<advisr_LookupSearchResult> searchGroups(String searchTerm, List<String> selectedIds){
    List<advisr_LookupSearchResult> results = new List<advisr_LookupSearchResult>();
    advisr_GroupSelector selector = new advisr_GroupSelector();
    List<Advisr_Group__c> groups = selector.searchAdvisrGroupsByName(searchTerm);
    for (Advisr_Group__c record : groups) {
      results.add(new advisr_LookupSearchResult(
        record.Id,
        'Advisr_Group__c',
        'standard:action_list_component',
        record.Advisr_Group_Name__c,
        ''
      ));
    }
    return results;
  }
}
