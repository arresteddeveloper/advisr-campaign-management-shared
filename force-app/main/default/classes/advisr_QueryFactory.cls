/*
 * Name          : advisr_QueryFactory
 * Author        : Shift CRM
 * Description   : Extremely simplified version of Financial Force QueryFactory using only a subset of the functionality needed
 *                 For future reference/expansion, can locate original code here https://github.com/apex-enterprise-patterns/fflib-apex-common/blob/master/sfdx-source/apex-common/main/classes/fflib_QueryFactory.cls
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
public with sharing class advisr_QueryFactory {
  public enum SortOrder {
    ASCENDING,
    DESCENDING
  }
  /**
   * This property is read-only and may not be set after instantiation.
   * The {@link Schema.SObjectType} token of the SObject that will be used in the FROM clause of the resultant query.
   **/
  public Schema.SObjectType table { get; private set; }
  @TestVisible
  private Set<String> fields;
  private String conditionExpression;
  private Integer limitCount;
  private List<Ordering> order;

  private Boolean sortSelectFields = true;

  /**
   * The relationship and  subselectQueryMap variables are used to support subselect queries.  Subselects can be added to
   * a query, as long as it isn't a subselect query itself.  You may have many subselects inside
   * a query, but they may only be 1 level deep (no subselect inside a subselect)
   * to add a subselect, call the subselectQuery method, passing in the ChildRelationship.
   **/
  private Schema.ChildRelationship relationship;
  private Map<Schema.ChildRelationship, advisr_QueryFactory> subselectQueryMap;

	private String getFieldPath(String fieldName){
		if(!fieldName.contains('.')){ //single field
			Schema.SObjectField token = table.getDescribe().fields.getMap().get(fieldName.toLowerCase());
			if(token == null)
				throw new InvalidFieldException(fieldName,this.table);
			return token.getDescribe().getName();
		}
    
    //traversing FK relationship(s)
		List<String> fieldPath = new List<String>();
		Schema.SObjectType lastSObjectType = table;
		Iterator<String> i = fieldName.split('\\.').iterator();
		while(i.hasNext()){
			String field = i.next().toLowerCase();
      Map<String, Schema.SObjectField> fieldMap = lastSObjectType.getDescribe().fields.getMap();
			Schema.SObjectField token = fieldMap.get((field.endsWithIgnoreCase('__r') ? //resolve custom field cross-object (__r) syntax
			(field.removeEndIgnoreCase('__r')+'__c') :
			field.toLowerCase()));
			
      if(token == null){
        token = fieldMap.get(field+'Id'); //in case it's a standard lookup in cross-object format
      }
      
      DescribeFieldResult tokenDescribe = token != null ? token.getDescribe() : null;
			

			if(token != null && i.hasNext() && tokenDescribe.getSoapType() == Schema.SoapType.ID){
				lastSObjectType = tokenDescribe.getReferenceTo()[0]; //if it's polymorphic doesn't matter which one we get
				fieldPath.add(tokenDescribe.getRelationshipName());
			}else if(token != null && !i.hasNext()){
				fieldPath.add(tokenDescribe.getName());
			}else{
				if(token == null)
					throw new InvalidFieldException(field,lastSObjectType);
				else
					throw new NonReferenceFieldException(lastSObjectType+'.'+field+' is not a lookup or master-detail field but is used in a cross-object query field.');
			}
		}

		return String.join(fieldPath,'.');
	}

  @TestVisible
	private static String getFieldTokenPath(Schema.SObjectField field){
		if(field == null){
			throw new InvalidFieldException('Invalid field: null');		
		}
		return field.getDescribe().getName();
	}

  public advisr_QueryFactory(Schema.SObjectType table) {
    this.table = table;
    this.fields = new Set<String>();
    this.order = new List<Ordering>();
  }

  /**
   * Construct a new advisr_QueryFactory instance with no options other than the FROM clause and the relationship.
   * This should be used when constructing a subquery query for addition to a parent query.
   * Objects created with this constructor cannot be added to another object using the subselectQuery method.
   * You *must* call selectField(s) before {@link #toSOQL} will return a valid, runnable query.
   * @param relationship the ChildRelationship to be used in the FROM Clause of the resultant Query (when set overrides value of table). This sets the value of {@link #relationship} and {@link #table}.
   **/
  private advisr_QueryFactory(Schema.ChildRelationship relationship) {
    this(relationship.getChildSObject());
    this.relationship = relationship;
  }

  /**
	 * Sets a flag to indicate that this query should have ordered
	 * query fields in the select statement (this at a small cost to performance).
	 * If you are processing large query sets, you should switch this off.
	 * @param doSort whether or not select fields should be sorted in the soql statement.
	 **/
	public advisr_QueryFactory setSortSelectFields(Boolean doSort){
		this.sortSelectFields = doSort;
		return this;
	}

  /**
	 * Selects multiple fields. This acts the same as calling {@link #selectField(String)} multiple times.
	 * @param fieldNames the Set of field API names to select.
	**/
	public advisr_QueryFactory selectFields(Set<String> fieldNames){
		for(String fieldName:fieldNames){
			fields.add( getFieldPath(fieldName) );
		}	
		return this;
	}
	/**
	 * Selects multiple fields. This acts the same as calling {@link #selectField(String)} multiple times.
	 * @param fieldNames the List of field API names to select.
	**/
	public advisr_QueryFactory selectFields(List<String> fieldNames){
		for(String fieldName:fieldNames)
			fields.add( getFieldPath(fieldName) );
		return this;
	}

  /**
	 * Selects multiple fields. This acts the same as calling {@link #selectField(Schema.SObjectField)} multiple times.
	 * @param fields the set of {@link Schema.SObjectField}s to select.
	 * @exception InvalidFieldException if the fields are null {@code fields}.
	**/
	public advisr_QueryFactory selectFields(Set<Schema.SObjectField> fields){
		for(Schema.SObjectField token:fields){
			if(token == null)
				throw new InvalidFieldException();
			this.fields.add( getFieldTokenPath(token) );
		}
		return this;
	}

  /**
	 * Selects multiple fields. This acts the same as calling {@link #selectField(Schema.SObjectField)} multiple times.
	 * @param fields the set of {@link Schema.SObjectField}s to select.
	 * @exception InvalidFieldException if the fields are null {@code fields}.	 
	**/
	public advisr_QueryFactory selectFields(List<Schema.SObjectField> fields){
		for(Schema.SObjectField token:fields){
			if(token == null)
				throw new InvalidFieldException();	
			this.fields.add( getFieldTokenPath(token) );
		}
		return this;
	}

  public advisr_QueryFactory selectField(Schema.SObjectField field) {
    this.fields.add(getFieldTokenPath(field));
    return this;
  }

  public advisr_QueryFactory selectField(String field) {
    this.fields.add(getFieldPath(field));
    return this;
  }

  /**
   * @param conditionExpression Sets the WHERE clause to the string provided. Do not include the "WHERE".
   **/
  public advisr_QueryFactory setCondition(String conditionExpression) {
    this.conditionExpression = conditionExpression;
    return this;
  }

  /**
	 * @returns the current value of the WHERE clause, if any, as set by {@link #setCondition}
	**/
	public String getCondition(){
		return this.conditionExpression;
	}

  /**
   * @param limitCount if not null causes a LIMIT clause to be added to the resulting query.
   **/
  public advisr_QueryFactory setLimit(Integer limitCount) {
    this.limitCount = limitCount;
    return this;
  }

  /**
	 * @returns the current value of the LIMIT clause, if any.
	**/
	public Integer getLimit(){
		return this.limitCount;
	}

  /**
	 * Add a field to be sorted on.  This may be a direct field or a field 
	 * related through an object lookup or master-detail relationship.
	 * Use the set to store unique field names, since we only want to sort
	 * by the same field one time.  The sort expressions are stored in a list
	 * so that they are applied to the SOQL in the same order that they
	 * were added in. 
	 * @param fieldName The string value of the field to be sorted on
	 * @param direction the direction to be sorted on (ASCENDING or DESCENDING)
	 * @param nullsLast whether to sort null values last (NULLS LAST keyword included).
	**/  
  public advisr_QueryFactory addOrdering(String fieldName, SortOrder direction, Boolean nullsLast){
		order.add(
			new Ordering(getFieldPath(fieldName), direction, nullsLast)
		);	
		return this;
  }

   /**
	 * Add a field to be sorted on.  This may be a direct field or a field 
	 * related through an object lookup or master-detail relationship.
	 * Use the set to store unique field names, since we only want to sort
	 * by the same field one time.  The sort expressions are stored in a list
	 * so that they are applied to the SOQL in the same order that they
	 * were added in. 
	 * The "NULLS FIRST" keywords will be included by default.  If "NULLS LAST" 
	 * is required, use one of the overloaded addOrdering methods which include this parameter.
	 * @param fieldName The string value of the field to be sorted on
	 * @param direction the direction to be sorted on (ASCENDING or DESCENDING)
	**/  
  public advisr_QueryFactory addOrdering(String fieldName, SortOrder direction){
		order.add(
			new Ordering(getFieldPath(fieldName), direction)
		);	
		return this;
  }

  /**
   * Add a field to be sorted on.  This may be a direct field or a field
   * related through an object lookup or master-detail relationship.
   * Use the set to store unique field names, since we only want to sort
   * by the same field one time.  The sort expressions are stored in a list
   * so that they are applied to the SOQL in the same order that they
   * were added in.
   * The "NULLS FIRST" keywords will be included by default.  If "NULLS LAST"
   * is required, use one of the overloaded addOrdering methods which include this parameter.
   * @param field The SObjectField to sort.  This can only be a direct reference.
   * @param direction the direction to be sorted on (ASCENDING or DESCENDING)
   **/
  public advisr_QueryFactory addOrdering(
    SObjectField field,
    SortOrder direction
  ) {
    order.add(new Ordering(field, direction));
    return this;
  }

  /**
   * Add a field to be sorted on.  This may be a direct field or a field
   * related through an object lookup or master-detail relationship.
   * Use the set to store unique field names, since we only want to sort
   * by the same field one time.  The sort expressions are stored in a list
   * so that they are applied to the SOQL in the same order that they
   * were added in.
   * The "NULLS FIRST" keywords will be included by default.  If "NULLS LAST"
   * is required, use one of the overloaded addOrdering methods which include this parameter.
   * @param field The SObjectField to sort.  This can only be a direct reference.
   * @param direction the direction to be sorted on (ASCENDING or DESCENDING)
   **/
  public advisr_QueryFactory addOrdering(
    SObjectField field,
    SortOrder direction,
    Boolean nullsLast
  ) {
    order.add(new Ordering(field, direction, nullsLast));
    return this;
  }

  /**
	 * @param o an instance of {@link advisr_QueryFactory.Ordering} to remove all existing (for instance defaults) and be added to the query's ORDER BY clause.
	 **/
	public advisr_QueryFactory addOrdering(Ordering o){
		this.order.add(o);
		return this;
	}

  /**
	 * @param o an instance of {@link advisr_QueryFactory.Ordering} to remove all existing (for instance defaults) and be added to the query's ORDER BY clause.
	 **/
	public advisr_QueryFactory setOrdering(Ordering o){
		this.order = new List<Ordering>{ o };
		return this;
	}

	/**
	 * @returns the list of orderings that will be used as the query's ORDER BY clause. You may remove elements from the returned list, or otherwise mutate it, to remove previously added orderings.
	**/
	public List<Ordering> getOrderings(){
		return this.order;
	}

  /**
	 * @returns the selected fields
	 **/
	public Set<String> getSelectedFields() { 
		return this.fields;
	}

  /**
   * Add a subquery query to this query.  If a subquery for this relationship already exists, it will be returned.
   * If not, a new one will be created and returned.
   * @param related The related object type
   **/
  public advisr_QueryFactory subselectQuery(SObjectType related) {
    return setSubselectQuery(getChildRelationship(related));
  }

  /**
   * Add a subquery query to this query.  If a subquery for this relationshipName already exists, it will be returned.
   * If not, a new one will be created and returned.
   * @param relationship The ChildRelationship to be added as a subquery
   **/
  public advisr_QueryFactory subselectQuery(String relationshipName) {
    Schema.ChildRelationship relationship = getChildRelationship(relationshipName);
		if (relationship != null) {
			return setSubselectQuery(relationship);
		}
		throw new InvalidSubqueryRelationshipException('Invalid call to subselectQuery with relationshipName = '+relationshipName +'.  Relationship does not exist for ' + table.getDescribe().getName());	
  }

  /**
	 * Add a subquery query to this query.  If a subquery for this relationshipName already exists, it will be returned.
	 * If not, a new one will be created and returned.
	 * @exception InvalidSubqueryRelationshipException If this method is called on a subselectQuery or with an invalid relationship 
	 * @param relationship The ChildRelationship to be added as a subquery
	**/
	public advisr_QueryFactory subselectQuery(Schema.ChildRelationship relationship){ 
		return setSubselectQuery(relationship);
	}

  /**
   * Add a subquery query to this query.  If a subquery for this relationship already exists, it will be returned.
   * If not, a new one will be created and returned.
   * @param relationship The ChildRelationship to be added as a subquery
   **/
  private advisr_QueryFactory setSubselectQuery(
    Schema.ChildRelationship relationship
  ) {
    if (this.relationship != null){
			throw new InvalidSubqueryRelationshipException('Invalid call to subselectQuery.  You may not add a subselect query to a subselect query.');
		}
    if (this.subselectQueryMap == null) {
      this.subselectQueryMap = new Map<Schema.ChildRelationship, advisr_QueryFactory>();
    }
    if (this.subselectQueryMap.containsKey(relationship)) {
      return subselectQueryMap.get(relationship);
    }

    advisr_QueryFactory subSelectQuery = new advisr_QueryFactory(relationship);

    //The child queryFactory should be configured in the same way as the parent by default - can override after if required
		subSelectQuery.setSortSelectFields(sortSelectFields);

    subselectQueryMap.put(relationship, subSelectQuery);
    return subSelectQuery;
  }

  /**
	 * @returns the list of subquery instances of advisr_QueryFactory which will be added to the SOQL as relationship/child/sub-queries.
	**/
	public List<advisr_QueryFactory> getSubselectQueries(){
		if (subselectQueryMap != null) {
			return subselectQueryMap.values();
		}	
		return null;
	}

  /**
   * Get the ChildRelationship from the Table for the object type passed in.
   * @param objType The object type of the child relationship to get
   **/
  private Schema.ChildRelationship getChildRelationship(SObjectType objType) {
    for (
      Schema.ChildRelationship childRow : table.getDescribe()
        .getChildRelationships()
    ) {
      //occasionally on some standard objects (Like Contact child of Contact) do not have a relationship name.
      //if there is no relationship name, we cannot query on it, so throw an exception.
      if (
        childRow.getChildSObject() == objType &&
        childRow.getRelationshipName() != null
      ) {
        return childRow;
      }
    }
    
    throw new InvalidSubqueryRelationshipException(
      'Invalid call to subselectQuery.  Invalid relationship for table ' +
      table +
      ' and objtype=' +
      objType
    );
  }

  /**
   * Get the ChildRelationship from the Table for the relationship name passed in.
   * @param relationshipName The name of the object's ChildRelationship on get
   **/
  private Schema.ChildRelationship getChildRelationship(
    String relationshipName
  ) {
    for (
      Schema.ChildRelationship childRow : table.getDescribe()
        .getChildRelationships()
    ) {
      if (childRow.getRelationshipName() == relationshipName) {
        return childRow;
      }
    }
    throw new InvalidSubqueryRelationshipException(
      'Invalid call to subselectQuery with relationshipName = ' +
      relationshipName +
      '.  Relationship does not exist for ' +
      table.getDescribe().getName()
    );
  }

  /**
	 * Remove existing ordering and set a field to be sorted on.  This may be a direct field or a field
	 * related through an object lookup or master-detail relationship.
	 * Use the set to store unique field names, since we only want to sort
	 * by the same field one time.  The sort expressions are stored in a list
	 * so that they are applied to the SOQL in the same order that they
	 * were added in.
	 * @param fieldName The string value of the field to be sorted on
	 * @param direction the direction to be sorted on (ASCENDING or DESCENDING)
	 * @param nullsLast whether to sort null values last (NULLS LAST keyword included).
	**/
	public advisr_QueryFactory setOrdering(String fieldName, SortOrder direction, Boolean nullsLast){
		Ordering ordr = new Ordering(getFieldPath(fieldName), direction, nullsLast);
		return setOrdering(ordr);
	}

	/**
	 * Remove existing ordering and set a field to be sorted on.  This may be a direct field or a field
	 * related through an object lookup or master-detail relationship.
	 * Use the set to store unique field names, since we only want to sort
	 * by the same field one time.  The sort expressions are stored in a list
	 * so that they are applied to the SOQL in the same order that they
	 * were added in.
	 * @param field The SObjectField to sort.  This can only be a direct reference.
	 * @param direction the direction to be sorted on (ASCENDING or DESCENDING)
	 * @param nullsLast whether to sort null values last (NULLS LAST keyword included).
	**/
	public advisr_QueryFactory setOrdering(SObjectField field, SortOrder direction, Boolean nullsLast){
		Ordering ordr = new Ordering(getFieldTokenPath(field), direction, nullsLast);
		return setOrdering(ordr);
	}

	/**
	 * Remove existing ordering and set a field to be sorted on.  This may be a direct field or a field
	 * related through an object lookup or master-detail relationship.
	 * Use the set to store unique field names, since we only want to sort
	 * by the same field one time.  The sort expressions are stored in a list
	 * so that they are applied to the SOQL in the same order that they
	 * were added in.
	 * @param fieldName The string value of the field to be sorted on
	 * @param direction the direction to be sorted on (ASCENDING or DESCENDING)
	**/
	public advisr_QueryFactory setOrdering(String fieldName, SortOrder direction){
		Ordering ordr = new Ordering(getFieldPath(fieldName), direction);
		return setOrdering(ordr);
	}

	/**
	 * Remove existing ordering and set a field to be sorted on.  This may be a direct field or a field
	 * related through an object lookup or master-detail relationship.
	 * Use the set to store unique field names, since we only want to sort
	 * by the same field one time.  The sort expressions are stored in a list
	 * so that they are applied to the SOQL in the same order that they
	 * were added in.
	 * @param field The SObjectField to sort.  This can only be a direct reference.
	 * @param direction the direction to be sorted on (ASCENDING or DESCENDING)
	**/
	public advisr_QueryFactory setOrdering(SObjectField field, SortOrder direction){
		Ordering ordr = new Ordering(getFieldTokenPath(field), direction);
		return setOrdering(ordr);
	}

  /**
   * Convert the values provided to this instance into a full SOQL string for use with Database.query
   * Check to see if subqueries queries need to be added after the field list.
   **/
  public String toSOQL() {
    String result = 'SELECT ';
    if (this.fields.size() == 0) {
      result += 'Id';
    } else {
      List<String> fieldsToQuery = new List<String>(this.fields);

      if(sortSelectFields){
				fieldsToQuery.sort(); 
			}
      result += String.join(fieldsToQuery, ', ');
    }

    if (subselectQueryMap != null && !subselectQueryMap.isEmpty()) {
      for (advisr_QueryFactory childRow : subselectQueryMap.values()) {
        result += ', (' + childRow.toSOQL() + ') ';
      }
    }

    result += ' FROM ' + (relationship != null ? relationship.getRelationshipName() : table.getDescribe().getName());

    if (conditionExpression != null)
      result += ' WHERE ' + conditionExpression;

    if (order.size() > 0) {
      result += ' ORDER BY ';
      for (Ordering o : order)
        result += o.toSOQL() + ', ';
      result = result.substring(0, result.length() - 2);
    }

    if (limitCount != null)
      result += ' LIMIT ' + limitCount;

    return result;
  }

  public class Ordering{
		private SortOrder direction;
		private boolean nullsLast;
		private String field;

		public Ordering(String sobjType, String fieldName, SortOrder direction){
			this(
				Schema.getGlobalDescribe().get(sobjType).getDescribe().fields.getMap().get(fieldName),
				direction
			);
		}
		/**
		 * Construct a new ordering instance for use with {@link advisr_QueryFactory#addOrdering}
		 * Once constructed it's properties may not be modified.
		**/
		public Ordering(Schema.SObjectField field, SortOrder direction){
			this(advisr_QueryFactory.getFieldTokenPath(field), direction, false); //SOQL docs state NULLS FIRST is default behavior
		}
		public Ordering(Schema.SObjectField field, SortOrder direction, Boolean nullsLast){
			this(advisr_QueryFactory.getFieldTokenPath(field), direction, nullsLast);
		}
		@TestVisible
		private Ordering(String field, SortOrder direction){
			this(field, direction, false);
		}
		@TestVisible
		private Ordering(String field, SortOrder direction, Boolean nullsLast){
			this.direction = direction;
			this.field = field;
			this.nullsLast = nullsLast;
		}
		public String getField(){
			return this.field;
		}
		public SortOrder getDirection(){
			return direction;
		}
		public String toSOQL(){
			return field + ' ' + (direction == SortOrder.ASCENDING ? 'ASC' : 'DESC') + (nullsLast ? ' NULLS LAST ' : ' NULLS FIRST ');
		}
	}


  public class InvalidFieldException extends Exception{
		private String fieldName;
		private Schema.SObjectType objectType;
		public InvalidFieldException(String fieldName, Schema.SObjectType objectType){
			this.objectType = objectType;
			this.fieldName = fieldName;
			this.setMessage( 'Invalid field \''+fieldName+'\' for object \''+objectType+'\'' );
		}
	}
	public class InvalidFieldSetException extends Exception{}
	public class NonReferenceFieldException extends Exception{}
	public class InvalidSubqueryRelationshipException extends Exception{}	
}
