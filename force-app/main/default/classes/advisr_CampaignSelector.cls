/*
 * Name          : advisr_CampaignSelector
 * Author        : Shift CRM
 * Description   : Apex class used to handle queries on the Advisr_Campaign__c object
 * TODO: May look to expand this in the future and create a more generic SObject Selector class that this can extend. Not necessary for now
 *
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
public class advisr_CampaignSelector extends advisr_SObjectSelector {
  /**
   * List of fields to be used in most query cases
   */
  private List<Schema.SObjectField> getSObjectFieldList() {
    return new List<Schema.SObjectField>{
      Advisr_Campaign__c.Advisr_Campaign_Name__c,
      Advisr_Campaign__c.Advisr_Campaign_Is_Primary__c,
      Advisr_Campaign__c.Advisr_Campaign_ID__c,
      Advisr_Campaign__c.Advisr_Campaign_Budget__c,
      Advisr_Campaign__c.CreatedDate,
      Advisr_Campaign__c.Opportunity__c,
      Advisr_Campaign__c.Advisr_Client__c
    };
  }

  public Schema.SObjectType getSObjectType() {
    return Advisr_Campaign__c.SobjectType;
  }

  /**
   * Retrieve All Advisr_Campaign__c records by Opportunity ID that are Advisr_Campaign_Is_Deleted__c = FALSE
   * NOTE: Ignores security
   * @param opportunityId Id of the opportunity to retrieve active campaigns for
   */
  public List<Advisr_Campaign__c> getActiveAdvisrCampaignsByOppId(
    Id opportunityId
  ) {
    return (List<Advisr_Campaign__c>) Database.query(
      newQueryFactory()
        .setCondition(
          'Opportunity__c = :opportunityId AND Advisr_Campaign_Is_Deleted__c = false'
        )
        .toSOQL()
    );
  }

  /**
   * Retrieve All Advisr_Campaign__c records by Opportunity ID
   * NOTE: Ignores security
   * @param opportunityId Id of the opportunity to retrieve campaigns for
   */
  public List<Advisr_Campaign__c> getAllAdvisrCampaignsByOppId(
    Id opportunityId
  ) {
    return (List<Advisr_Campaign__c>) Database.query(
      newQueryFactory().setCondition('Opportunity__c = :opportunityId').toSOQL()
    );
  }

  /**
   * Retrieve a list of remaining active campaigns on an opportunity prior to unlinking some
   * @param opportunityId The opportunity to check for any remaining campaigns
   * @param unlinkingCampaignIds List of ID records that will be unlinked. These will be filtered from the query
   */
  public List<Advisr_Campaign__c> getRemainingActiveCampaigns(
    Id opportunityId,
    List<Id> unlinkingCampaignIds
  ) {
    return (List<Advisr_Campaign__c>) Database.query(
      newQueryFactory()
        .setCondition(
          'Opportunity__c = :opportunityId AND Id NOT IN :unlinkingCampaignIds AND Advisr_Campaign_Is_Deleted__c = false'
        )
        .toSOQL()
    );
  }

  /**
   * Search through the list of campaigns by campaign name, filtering by client additionally
   * @param searchTerm String value to search on the Advisr_Campaign_Name__c field
   * @param clientId Id of the Client to filter the campaigns by further
   * @param opportunityId Id of the opportunity that should be filtered out of the query
   */
  public List<Advisr_Campaign__c> searchCampaignsByNameAndClient(
    String searchTerm,
    String clientId,
    String opportunityId
  ) {
    if (searchTerm != null) searchTerm = '%' + searchTerm + '%';
    
    advisr_QueryFactory campaignFactory = newQueryFactory()
      .setLimit(100);
    
    String condition = searchTerm != null ? 'Advisr_Client__c = :clientId AND Advisr_Campaign_Name__c LIKE :searchTerm' : 'Advisr_Client__c = :clientId';
    condition += ' AND Opportunity__c != :opportunityId';

    return Database.query(
      campaignFactory
        .setCondition(condition)
        .toSOQL()
    );
  }
}
