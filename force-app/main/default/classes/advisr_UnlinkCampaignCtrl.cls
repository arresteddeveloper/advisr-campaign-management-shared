/*
 * Name          : advisr_UnlinkCampaignCtrl
 * Author        : Shift CRM
 * Description   : Apex class used by the advisr_UnlinkCampaign LWC
 *
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
public with sharing class advisr_UnlinkCampaignCtrl {
  @AuraEnabled(cacheable=true)
  public static List<Advisr_Campaign__c> getAdvisrCampaignsById(
    List<Id> campaignIds
  ) {
    return new advisr_CampaignSelector().selectSObjectsById(campaignIds);
  }

  @AuraEnabled(cacheable=true)
  public static List<Advisr_Campaign__c> getRemainingActiveCampaigns(
    Id opportunityId,
    List<Id> unlinkingCampaignIds
  ) {
    return new advisr_CampaignSelector().getRemainingActiveCampaigns(
      opportunityId,
      unlinkingCampaignIds
    );
  }

  @AuraEnabled
  public static void unlinkCampaigns(
    List<Advisr_Campaign__c> campaignsToUnlink,
    Advisr_Campaign__c replacementPrimaryCampaign
  ) {
    advisr_CampaignService.unlinkCampaigns(
      campaignsToUnlink,
      replacementPrimaryCampaign
    );
  }
}
