/**
 * Copyright (c), FinancialForce.com, inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 * - Neither the name of the FinancialForce.com, inc nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

/*
 * Name          : advisr_SObjectSelector
 * Author        : Shift CRM
 * Description   : Abstract class used for creating selector classes used for querying the database
 *                 For future reference/expansion, can locate original code here https://github.com/apex-enterprise-patterns/fflib-apex-common/blob/master/sfdx-source/apex-common/main/classes/fflib_SObjectSelector.cls
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
public abstract with sharing class advisr_SObjectSelector {
  /**
   * Indicates whether the sObject has the currency ISO code field for organisations which have multi-currency
   * enabled.
   **/
  private Boolean CURRENCY_ISO_CODE_ENABLED {
    get {
      if (CURRENCY_ISO_CODE_ENABLED == null) {
        CURRENCY_ISO_CODE_ENABLED = Schema.getGlobalDescribe()
          .containsKey('CurrencyType');
      }
      return CURRENCY_ISO_CODE_ENABLED;
    }
    set;
  }

  /**
   * Order by field
   **/
  private String m_orderBy;

  /**
   * Sort the query fields in the select statement (defaults to true, at the expense of performance).
   * Switch this off if you need more performant queries.
   **/
  private Boolean m_sortSelectFields = true;
  private Map<String, Schema.SObjectField> m_fields;

  /**
   * static variables
   **/
  private static String DEFAULT_SORT_FIELD = 'CreatedDate';
  private static String SF_ID_FIELD = 'Id';

  private Schema.SObjectField nameField;

  /**
   * Implement this method to inform the base class of the SObject (custom or standard) to be queried
   **/
  abstract Schema.SObjectType getSObjectType();

  /**
   * Implement this method to inform the base class of the common fields to be queried or listed by the base class methods
   **/
  abstract List<Schema.SObjectField> getSObjectFieldList();

  /**
   * Override this method to control the default ordering of records returned by the base queries,
   * defaults to the name field of the object if it is not encrypted or CreatedDate if there the object has createdDated or Id
   **/
  public virtual String getOrderBy() {
    if (m_orderBy == null) {
      Schema.SObjectField nameField = getNameField();
      if (nameField != null && !nameField.getDescribe().isEncrypted()) {
        m_orderBy = nameField.getDescribe().getName();
      } else {
        m_orderBy = DEFAULT_SORT_FIELD;
        try {
          if (getFieldMap().get(m_orderBy.toLowerCase()) == null) {
            m_orderBy = SF_ID_FIELD;
          }
        } catch (advisr_QueryFactory.InvalidFieldException ex) {
          m_orderBy = SF_ID_FIELD;
        }
      }
    }
    return m_orderBy;
  }

  /**
   * Returns the field where isNameField() is true (if any); otherwise returns null
   **/
  public Schema.SObjectField getNameField() {
    if (nameField == null) {
      for (Schema.SObjectField field : getFieldMap().values()) {
        if (field.getDescribe().isNameField()) {
          nameField = field;
          break;
        }
      }
    }
    return nameField;
  }

  /**
   * Get the fields associated with the table for the selector
   */
  public Map<String, Schema.SObjectField> getFieldMap() {
    if (m_fields == null) {
      m_fields = getSObjectType().getDescribe().fields.getMap();
    }

    return m_fields;
  }

  /**
   * Constructs the Selector
   *
   * @param includeFieldSetFields Set to true if the Selector queries are to include Fieldset fields as well
   **/
  public advisr_SObjectSelector() {
  }

  public advisr_SObjectSelector(Boolean sortSelectFields) {
    m_sortSelectFields = sortSelectFields;
  }

  /**
   * Returns the string representation of the SObject this selector represents
   **/
  public String getSObjectName() {
    return getSObjectType().getDescribe().getName();
  }

  /**
   * Performs a SOQL query,
   *   - Selecting the fields described via getSObjectFieldsList and getSObjectFieldSetList (if included)
   *   - From the SObject described by getSObjectType
   *   - Where the Id's match those provided in the set
   *   - Ordered by the fields returned via getOrderBy
   * @returns A list of SObject's
   **/
  public virtual List<SObject> selectSObjectsById(Set<Id> idSet) {
    return Database.query(buildQuerySObjectById());
  }

  /**
   * Performs a SOQL query,
   *   - Selecting the fields described via getSObjectFieldsList and getSObjectFieldSetList (if included)
   *   - From the SObject described by getSObjectType
   *   - Where the Id's match those provided in the List
   *   - Ordered by the fields returned via getOrderBy
   * @returns A list of SObject's
   **/
  public virtual List<SObject> selectSObjectsById(List<Id> idList) {
    Set<Id> idSet = new Set<Id>();
    idSet.addAll(idList);
    return selectSObjectsById(idSet);
  }

  /**
   * Performs a SOQL query,
   *   - Selecting the fields described via getSObjectFieldsList and getSObjectFieldSetList (if included)
   *   - From the SObject described by getSObjectType
   *   - Where the Id's match those provided in the set
   *   - Ordered by the fields returned via getOrderBy
   * @returns A QueryLocator (typically for use in a Batch Apex job)
   **/
  public virtual Database.QueryLocator queryLocatorById(Set<Id> idSet) {
    return Database.getQueryLocator(buildQuerySObjectById());
  }

  /**
   * Public access for the getSObjectType during Mock registration
   *   (adding public to the existing method broken base class API backwards compatibility)
   **/
  public SObjectType getSObjectType2() {
    return getSObjectType();
  }

  /**
   * Public access for the getSObjectType during Mock registration
   *   (adding public to the existing method broken base class API backwards compatibility1)
   **/
  public SObjectType sObjectType() {
    return getSObjectType();
  }

  /**
   * Returns a QueryFactory configured with the Selectors object, fields and default order by
   **/
  public advisr_QueryFactory newQueryFactory() {
    return newQueryFactory(true);
  }

  /**
   * Returns a QueryFactory configured with the Selectors object, fields and default order by
   **/
  public advisr_QueryFactory newQueryFactory(Boolean includeSelectorFields) {
    // Construct QueryFactory around the given SObject
    return configureQueryFactory(
      new advisr_QueryFactory(getSObjectType2()),
      includeSelectorFields
    );
  }

  public advisr_QueryFactory getRecentlyViewed() {
    advisr_QueryFactory factory = new advisr_QueryFactory(
        RecentlyViewed.SobjectType
      )
      .selectFields(
        new List<Schema.SObjectField>{ RecentlyViewed.Id, RecentlyViewed.Name }
      )
      .setCondition('Type = \'' + this.sObjectType() + '\'');

    System.debug('Query so far >> ' + factory.toSOQL());
    return factory;
  }

  /**
   * Constructs the default SOQL query for this selector, see selectSObjectsById and queryLocatorById
   **/
  private String buildQuerySObjectById() {
    return newQueryFactory().setCondition('id in :idSet').toSOQL();
  }

  /**
   * Adds a subselect QueryFactory based on this selector to the given QueryFactor, returns the parentQueryFactory
   **/
  public advisr_QueryFactory addQueryFactorySubselect(
    advisr_QueryFactory parentQueryFactory
  ) {
    return addQueryFactorySubselect(parentQueryFactory, true);
  }

  /**
   * Adds a subselect QueryFactory based on this selector to the given QueryFactor
   **/
  public advisr_QueryFactory addQueryFactorySubselect(
    advisr_QueryFactory parentQueryFactory,
    Boolean includeSelectorFields
  ) {
    advisr_QueryFactory subSelectQueryFactory = parentQueryFactory.subselectQuery(
      getSObjectType2()
    );
    return configureQueryFactory(subSelectQueryFactory, includeSelectorFields);
  }

  /**
   * Adds a subselect QueryFactory based on this selector to the given QueryFactor, returns the parentQueryFactory
   **/
  public advisr_QueryFactory addQueryFactorySubselect(
    advisr_QueryFactory parentQueryFactory,
    String relationshipName
  ) {
    return addQueryFactorySubselect(parentQueryFactory, relationshipName, true);
  }

  /**
   * Adds a subselect QueryFactory based on this selector to the given QueryFactor
   **/
  public advisr_QueryFactory addQueryFactorySubselect(
    advisr_QueryFactory parentQueryFactory,
    String relationshipName,
    Boolean includeSelectorFields
  ) {
    advisr_QueryFactory subSelectQueryFactory = parentQueryFactory.subselectQuery(
      relationshipName
    );
    return configureQueryFactory(subSelectQueryFactory, includeSelectorFields);
  }

  /**
   * Configures a QueryFactory instance according to the configuration of this selector
   **/
  private advisr_QueryFactory configureQueryFactory(
    advisr_QueryFactory queryFactory,
    Boolean includeSelectorFields
  ) {
    // Configure the QueryFactory with the Selector fields?
    if (includeSelectorFields) {
      // select the Selector fields and set order
      queryFactory.selectFields(getSObjectFieldList());

      // Automatically select the CurrencyIsoCode for MC orgs (unless the object is a known exception to the rule)
      if (CURRENCY_ISO_CODE_ENABLED && UserInfo.isMultiCurrencyOrganization())
        queryFactory.selectField('CurrencyIsoCode');
    }

    // Parse the getOrderBy()
    for (String orderBy : getOrderBy().split(',')) {
      List<String> orderByParts = orderBy.trim().split(' ');
      String fieldNamePart = orderByParts[0];
      String fieldSortOrderPart = orderByParts.size() > 1
        ? orderByParts[1]
        : null;
      advisr_QueryFactory.SortOrder fieldSortOrder = advisr_QueryFactory.SortOrder.ASCENDING;
      if (fieldSortOrderPart == null)
        fieldSortOrder = advisr_QueryFactory.SortOrder.ASCENDING;
      else if (fieldSortOrderPart.equalsIgnoreCase('DESC'))
        fieldSortOrder = advisr_QueryFactory.SortOrder.DESCENDING;
      else if (fieldSortOrderPart.equalsIgnoreCase('ASC'))
        fieldSortOrder = advisr_QueryFactory.SortOrder.ASCENDING;
      queryFactory.addOrdering(
        fieldNamePart,
        fieldSortOrder,
        orderBy.containsIgnoreCase('NULLS LAST')
      );
    }

    queryFactory.setSortSelectFields(m_sortSelectFields);

    return queryFactory;
  }
}
