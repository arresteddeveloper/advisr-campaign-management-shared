/*
 * Name          : advisr_CampaignSelector
 * Author        : Shift CRM
 * Description   : Apex class used to unlink campaign records.
 * TODO: May look to expand this in the future and create a more generic SObject Selector class that this can extend. Not necessary for now
 *
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/27/2021     Bryce Batson     0.1       Initial
 */
public class advisr_CampaignService {

  /**
   * Accepts a list of campaigns that should be removed from an opportunity, along with an optional campaign that will act as a replacement primary
   * @param campaignsToUnlink List of campaigns that should be "Unlinked" from the Opportunity (Opportunity__c value cleared and primary flag toggled false)
   * @param replacementPrimaryCampaign Campaign that should be used as a replacement (optionally) where it will be marked Advisr_Campaign_Is_Primary__c = true
   */
  public static void unlinkCampaigns(
    List<Advisr_Campaign__c> campaignsToUnlink,
    Advisr_Campaign__c replacementPrimaryCampaign
  ) {
    List<Advisr_Campaign__c> campaignsToUpdate = new List<Advisr_Campaign__c>();

    //Loop through all of the campaigns to unlink and rset the Opportunity
    for (Advisr_Campaign__c campaign : campaignsToUnlink) {
      campaign.Opportunity__c = null;
      campaign.Advisr_Campaign_Is_Primary__c = false;
      campaignsToUpdate.add(campaign);
    }

    //If there is a replacement campaign, make sure to set it as primary
    if (replacementPrimaryCampaign != null) {
      replacementPrimaryCampaign.Advisr_Campaign_Is_Primary__c = true;
      campaignsToUpdate.add(replacementPrimaryCampaign);
    }

    if (!campaignsToUpdate.isEmpty()) Database.update(campaignsToUpdate);
  }
}
