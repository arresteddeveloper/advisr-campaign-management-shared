/*
 * Name          : advisr_CampaignTrigger
 * Author        : Shift CRM
 * Description   : Apex trigger for the Advisr_Campaign__c object
 *
 * Maintenance History:
 * Date ------------ Name  ----  Version --- Remarks
 * 1/29/2021     Ryan Morden       0.1       Initial
 */
trigger advisr_CampaignTrigger on advisr_Campaign__c(
  before insert,
  before update,
  before delete,
  after insert,
  after update,
  after delete
) {
  if (Trigger.isBefore) {
    if (Trigger.isInsert) {
      advisr_PrimaryCampaignHandler.routeLogic(trigger.new, new Map<Id, advisr_Campaign__c>());
    } else if (Trigger.isUpdate) {
      advisr_PrimaryCampaignHandler.routeLogic(trigger.new, trigger.oldmap);
    } else if (Trigger.isDelete) {
    }
  } else if (Trigger.isAfter) {
    if (Trigger.isInsert) {
    } else if (Trigger.isUpdate) {
    } else if (Trigger.isDelete) {
    }
  }

}