const { jestConfig } = require('@salesforce/sfdx-lwc-jest/config');
module.exports = {
  ...jestConfig,
  moduleNameMapper: {
    '^lightning/datatable$': '<rootDir>/force-app/test/jest-mocks/lightning/datatable',
    '^lightning/uiObjectInfoApi$': '<rootDir>/force-app/test/jest-mocks/lightning/uiObjectInfoApi'
  }
}